# System design and architecture

## Modules

- **astrovoyageur** - the game
  - **scripts** - scripts for the game scene
- **ludmotoro** - the engine
  - **game** - a common scene graph and scripting system
    - **misc** - misc game objects and scene utilities
    - **physics** - everything needed for 2D physics
  - **graphics** - an abstract 2D renderer and the rest of computer graphics
    - **lwjgl** - an LWJGL implementation of the renderer
    - **ui**
  - **math**
  - **utils** - helper classes and what not

# Code style

## Replacement names for "TODO"

In order of consideration:
- **BUG** for runtime-wrong code to be fixed
- **HACK** for wrong/botched/incomplete code to be fixed
- **TODO** for ideas to be implemented
- **IDEA** for not necessary ideas to be thought over
- **XXX** for when you can't choose any from the above

## Naming style

Functions with side-effects (except logs, stats etc.) should have names in form of an imperative clause. Variables and functions without side-effects should have names in form of a noun with the exception of boolean-returning ones, their names should be in form of a yes-no question.

Use `to` for lossy conversions and `as` for lossless conversions. Use `from` for object creation functions with parameters.

Reserved keywords, setter/getter-guarded variables and temporary constructor/function parameters ought to be prefixed with `_`.

Cases:
- Functions and variables: `snake_case`
- Types, objects and enum values: `PascalCase`

## Miscellaneous rules

- Interfaces and implementations should be `trait`s, and the rest `class`es.
- Interfaces should be postfixed with `Like` and implementations `Impl`.
- Classes you don't want to be instantiated should be `abstract`, simple data storage classes should be `case`.
- Prefer overriding methods over passing function objects for callbacks.
- Max line width is 120 chars.
- Import `collection.mutable` or `collection.immutable` for mutable/immutable collections from the standard library.
- Always put a comma after every line in multiline array-like expressions of unbounded size.
- Always remove empty argument lists from function calls and decls/defs without side-effect.
- Empty function bodies must be folded. \
  **GOOD**
  ```
  def do_nothing() = {}
  ```
  **BAD**
  ```
  def do_nothing() = {
  }
  ```
- Always put a fractional part in numbers in floating-point operations \
  **GOOD**
  ```
  val width = 800
  width / 2.5
  val ten = 10.0
  ten / 10.0, ten * 10.0
  ```
  **BAD**
  ```
  val ten = 10.0
  ten / 10, ten * 10
  ```
- Group imports by library, sort all those groups by dependence order, then sort imports inside these groups alphabetically by package path, and then sort each list of imported symbols. \
  **GOOD**
  ```
  import scala.Stuff
  import scala.alphabet.{A, B, C}
  import scala.stuff.{ABC, something, X, XYZ, y, Z}

  import lwjgl.OpenGL
  import lwjgl.opengl.{one, three, two}
  import lwjgl.package1

  import engine.Object
  import renderer.i_need_lwjgl
  ```
- When the complexity of an internal component rises beyond your control, create a mini sub-interface in it - that is, start naming method based not on their effects but based on their intentions. This will (probably) reduce the complexity of your code. An example of the application of this rule is my implementation of a segment tree (https://github.com/digitcrusher/algorytmy/).
- Omit `scala.` in imports from the standard library.
- Separate long numbers into groups of three digits starting from the decimal point.
- Prefer collections methods over complex for loops
- Use `i`, `j` and so on in for loops where the binding's meaning can be immediately deduced from the generator expression and its use is short-lived.
- `Some(…)` for Iterables with a single element and `Nil` for empty ones.
- Always move `extends` clauses to the next line with two indent levels, if the constructor is broken into multiple lines.
