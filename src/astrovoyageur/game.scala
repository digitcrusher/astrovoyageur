/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package astrovoyageur

import math.{Pi, tan, toRadians}
import util.Random

import ludmotoro.game.{CameraObject, RendererObject}
import ludmotoro.game.misc.{Side, SimpleTerrainGen, Terrain, TerrainGen}
import ludmotoro.game.physics.{Axis, PhysScene}
import ludmotoro.graphics.{Camera, Colors, DrawShapes, DrawText, DrawUploadedShape, Font, Renderer, WindowLike}
import ludmotoro.math.{AngleSpring, Vec2}

import astrovoyageur.scripts.{AabbDrawer, BallControl, CameraControl, ExtraKeys, Gravity, StatsText, TheVoid}

class MaxTwoTerrainGen(var terrain_gen_1: TerrainGen, var terrain_gen_2: TerrainGen) extends TerrainGen:
  def calc_node(side: Side): Vec2 =
    terrain_gen_1.terrain = terrain
    terrain_gen_2.terrain = terrain
    val node1 = terrain_gen_1.calc_node(side)
    val node2 = terrain_gen_2.calc_node(side)
    Vec2(node1.x, node1.y max node2.y)

object Priorities:
  val aabb_drawer = -1
  val other = 0
  val hud = 1

class GameScene(val renderer: Renderer & DrawShapes & DrawText & DrawUploadedShape & WindowLike,
                val fonts: Map[String, Font])
    extends PhysScene(Axis.X):

  var is_paused = false

  val camera = Camera()

  val stats_text = StatsText(this, renderer, fonts("ternary"), 15.0, Colors.white, false)
  stats_text.priority = Priorities.hud

  add_objects(RendererObject(renderer), CameraObject(camera), stats_text)

  val node_spacing = 20.0
  val low_slope = tan(toRadians(15.0))
  val low_smoothness = 0.8
  val low_min_y = -5.0
  val low_max_y = 5.0
  val high_slope = tan(toRadians(60.0))
  val high_smoothness = 0.5
  val high_min_y = -200.0
  val high_max_y = 200.0

  val low_lands = SimpleTerrainGen(
    -low_slope, low_slope,
    low_smoothness,
    node_spacing,
    Some(low_min_y), Some(low_max_y),
    should_get_nodes_from_terrain = false,
  )

  val high_lands = SimpleTerrainGen(
    -high_slope, high_slope,
    high_smoothness,
    node_spacing,
    Some(high_min_y), Some(high_max_y),
    should_get_nodes_from_terrain = false,
  )

  val terrain = Terrain(MaxTwoTerrainGen(low_lands, high_lands), 1.0)

  val aabb_drawer = AabbDrawer(false)
  aabb_drawer.priority = Priorities.aabb_drawer
  val gravity = Gravity(100.0, 500.0)
  val the_void = TheVoid(-10_000.0)

  add_objects(terrain, aabb_drawer, gravity, the_void)

  //val laser = TrackingLaser(Vec2.origin, AngleSpring(Pi * 1_000.0, 0.8, 1.0, true, 0.0, 0.0, 0.0), 50, None)
  add_objects(
    BallControl(Random.between(3.0, 7.0), 100.0, is_visible = false),
    CameraControl(camera, gravity),
    //laser,
    //TrackingLaserControl(laser),
    ExtraKeys(),
  )

  override def update_forcefully(delta_time: Double) =
    if is_paused then return
    time += delta_time * time_scale

    renderer.push_transform(camera.transform, camera.inverse_transform)

    integrate(delta_time * time_scale)
    detect_collisions()

    for i <- objects if i.ok do
      i.update(delta_time * time_scale)

    renderer.pop_transform()

  override def draw_forcefully(delta_time: Double) =
    val (ingame, hud) = objects.partition(_.priority < Priorities.hud)

    renderer.push_transform(camera.transform, camera.inverse_transform)

    for i <- ingame if i.ok do
      i.draw(delta_time)

    renderer.pop_transform()

    for i <- hud if i.ok do
      i.draw(delta_time)
