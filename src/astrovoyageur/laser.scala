/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package astrovoyageur

import math.Pi

import ludmotoro.game.{InteractiveScript, Object, renderers}
import ludmotoro.game.misc.Terrain
import ludmotoro.game.physics.{Body, PhysScene, Ray, Shape}
import ludmotoro.graphics.{Button, Colors, DrawShapes, Polygon, Renderer, WindowLike, WindowState}
import ludmotoro.math.{AngleSpring, Vec2, given}

class Laser(var pos: Vec2, var ori: AngleSpring, var bouncec: Int) extends Object:
  override def update(delta_time: Double) =
    ori.step(delta_time)

  override def draw(delta_time: Double) =
    val ray_body = new Body(Some(Ray(Vec2.origin, 0.0)), 1.0, pos, ori.pos):
      override def should_collide_with(body: Body) = !body.isInstanceOf[Terrain]

    var bounce_num = 0
    var has_collided = true
    var last_collided: Option[Shape] = None
    while has_collided do
      has_collided = false

      var new_pos = Vec2.origin
      var new_ori = 0.0

      if bounce_num < bouncec then
        var new_last_collided = last_collided
        var is_first = true

        for collision <- scene.asInstanceOf[PhysScene].collisions_with(ray_body)
            contact <- collision.contacts do
          val point = contact.point
          val (normal, b) =
            if ray_body.shape.contains(contact.a) then
              (contact.normal_for_a, contact.b)
            else
              (contact.normal_for_b, contact.a)

          if !(last_collided.nonEmpty && b == last_collided.get) &&
             (is_first || ray_body.pos.sqr_dist_to(point) < ray_body.pos.sqr_dist_to(new_pos)) then
            has_collided = true
            new_pos = point
            new_ori = 2.0 * normal.angle - ray_body.ori - Pi
            new_last_collided = Some(b)
            is_first = false

        bounce_num += 1
        last_collided = new_last_collided

      for renderer <- scene.renderers[DrawShapes] do
        val viewport = renderer.viewport

        if has_collided then
          renderer.draw(Polygon.from_line(ray_body.pos, new_pos, Colors.red.copy(saturation = 0.8)))
        else
          val radius_sqr = viewport.width * viewport.height
          val inf_ray_line_len = radius_sqr + ((viewport.center.sqr_dist_to(ray_body.pos) - radius_sqr) max 0)
          renderer.draw(Polygon.from_line(
            ray_body.pos, ray_body.pos + Vec2.polar(inf_ray_line_len, ray_body.ori),
            Colors.red.copy(saturation = 0.8)
          ))

      ray_body.pos = new_pos
      ray_body.ori = new_ori

class TrackingLaser(_pos: Vec2, _ori: AngleSpring, _bouncec: Int, var tracked_body: Option[Body])
    extends Laser(_pos, _ori, _bouncec):

  override def update(delta_time: Double) =
    tracked_body.foreach: body =>
      if body.ok then
        ori.target = pos.angle_to(body.pos)
      else
        tracked_body = None
    ori.step(delta_time)

class TrackingLaserControl(var laser: TrackingLaser) extends InteractiveScript:
  override def on_update(delta_time: Double) =
    for i <- scene.renderers[WindowLike] do
      if i.is_button_pressed(Button.KeyR) then
        val mouse_pos = i.untransform_pos(i.mouse_pos)

        if i.is_button_pressed(Button.KeyLeftShift) then
          laser.ori.target = laser.pos.angle_to(mouse_pos)
        else if !i.is_button_pressed(Button.KeyLeftCtrl) && !i.is_button_pressed(Button.KeyLeftAlt) then
          laser.pos = mouse_pos

  override def on_button_press(button: Button, past: WindowState, renderer: Renderer & WindowLike) =
    if button == Button.KeyR then
      if past.is_button_pressed(Button.KeyLeftCtrl) then
        laser.tracked_body = None

      else if past.is_button_pressed(Button.KeyLeftAlt) then
        val mouse_pos = renderer.untransform_pos(past.mouse_pos)
        var closest_dist = Double.PositiveInfinity
        for i <- scene.objects_of[Body] if i.ok do
          if !i.is_static then
            val dist = i.pos.sqr_dist_to(mouse_pos)
            if dist < closest_dist then
              closest_dist = dist
              laser.tracked_body = Some(i)
