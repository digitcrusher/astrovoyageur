/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package astrovoyageur

import ludmotoro.game.{RendererObject, Scene}
import ludmotoro.graphics.{Button, ClearScreen, Colors, Event, Rgb}
import ludmotoro.graphics.lwjgl.{GlDrawShapes, GlDrawText, GlUploadShapes, LwjglRenderer, StbFont}
import ludmotoro.graphics.ui
import ludmotoro.math.{Transforms2, Vec2}

// TODO: config
// TODO: audio
// TODO: demo recorder
// TODO: scripting and console
// IDEA: ai
// HACK: complete the ui
// HACK: seperate the ui
// TODO: support for scala native
// TODO: object editor "Sudo"
// TODO: a faster build system
// IDEA: logger

@main
def main() =
  MainScene().loop()

class MainScene extends Scene:
  val renderer = new LwjglRenderer(Vec2(800, 600), "Astrovoyageur") with GlDrawShapes with GlDrawText with GlUploadShapes
  renderer.init()

  // HACK: add bold and italic variants of fonts
  val fonts = Map(
    "primary"   -> StbFont("res/Jura-Medium.ttf", "Jura Medium Regular"),        // has polish and russian chars
    "secondary" -> StbFont("res/Offside-Regular.ttf", "Offside Regular"),        // ascii only
    "ternary"   -> StbFont("res/TurretRoad-Regular.ttf", "Turret Road Regular"), // has polish chars
  )

  add_object(RendererObject(renderer))

  var game_scene: Option[GameScene] = None
  /*
  var game_updatec = 0
  import ludmotoro.graphics.WindowState, collection.mutable
  var game_demo = mutable.Map[Int, (WindowState, mutable.Queue[Event])]()
  */

  def new_game() =
    quit_game()

    val value = GameScene(renderer, fonts)
    value.priority = -1
    add_object(value)
    game_scene = Some(value)
    //game_updatec = 0

    ups = value.ups
    dps = value.dps
    update_skip_time = value.update_skip_time

  def quit_game() =
    game_scene.foreach:
      _.delete()
    game_scene = None

  def resume_game() =
    game_scene.get.last_update = 0.0
    game_scene.get.is_paused = false

  def pause_game() =
    game_scene.get.is_paused = true

  def is_ui_visible = game_scene.forall(_.is_paused)

  override def update_forcefully(delta_time: Double) =
    time += delta_time * time_scale

    val all_events = renderer.events.toSeq
    renderer.events.clear()
    for event <- all_events do
      event match
        case Event.Close =>
          is_running = false

        case Event.ButtonPress(button) =>
          button match
            case Button.KeyEscape =>
              game_scene.foreach: x =>
                if x.is_paused then
                  resume_game()
                else
                  pause_game()
            case _ =>
        case _ =>

      if is_ui_visible then
        renderer.push_transform(Transforms2.move(renderer.viewport.start))
        pretty_ui.handle_event(event)(using renderer)
        renderer.pop_transform()
      else
        renderer.events += event

    /*
    if game_scene.nonEmpty then
      val (window_state, events) = game_demo.getOrElseUpdate(game_updatec, (renderer.window_state, renderer.events))
      val prev = renderer.window_state
      renderer.window_state = window_state
      renderer.events = mutable.Queue.from(events)
      game_updatec += 1
    */

    for i <- objects if i.ok do
      i.update(delta_time * time_scale)

    /*
      renderer.window_state = prev
    */

    renderer.events.clear()

  override def draw_forcefully(delta_time: Double) =
    renderer.begin()

    renderer.draw(ClearScreen(Rgb(0.1)))

    /*
    val prev = renderer.window_state
    game_demo.get(game_updatec).foreach: x =>
      renderer.window_state = x._1
    */

    for i <- objects if i.ok do
      i.draw(delta_time)

    /*
    renderer.window_state = prev
    */

    if is_ui_visible then
      renderer.push_transform(Transforms2.move(renderer.viewport.start))
      renderer.draw(ClearScreen(Rgb(0.0, 0.5)))
      pretty_ui.draw()(using renderer)
      renderer.pop_transform()

    renderer.end()

  val pretty_x_margin = 2.0
  val pretty_y_margin = 0.0
  val pretty_color = Colors.white
  val pretty_hovered_color = Rgb(0.7)
  val pretty_font = "primary"

  trait Button extends ui.Element:
    var is_hovered = false

    override def draw()(using renderer: ui.UiRenderer) =
      val old_is_hovered = is_hovered
      is_hovered = is_pos_on_abs_this(renderer.untransform_pos(renderer.mouse_pos))
      if old_is_hovered ^ is_hovered then
        if is_hovered then
          on_enter()
        else
          on_leave()

      super.draw()

    override def on_event(event: Event)(using ui.UiRenderer) =
      event match
        case Event.ButtonRelease(button) =>
          if button == Button.MouseLeft then
            on_release()
        case _ =>

    def on_release() = {}
    def on_enter() = {}
    def on_leave() = {}

  def pretty_main_menu: ui.Element =
    val menu = new ui.List(ui.Direction.Down) with ui.MinWidth with ui.MinHeight

    def menu_button(string: String)(on_release_func: => Unit) =
      val label = new ui.Label(string, fonts(pretty_font), 25.0, pretty_color) with ui.MinWidth with ui.MinHeight
      val result = new ui.Offset(label) with ui.Center with ui.MaxWidth with ui.MinHeight with ui.FixedWidthMargin with ui.FixedHeightMargin with Button:
        override def on_release() =
          on_release_func
        override def on_enter() =
          label.color = pretty_hovered_color
        override def on_leave() =
          label.color = pretty_color
      result.width_margin = pretty_x_margin * 2.0
      result.height_margin = pretty_y_margin * 2.0
      menu.add_elem(result)

    if game_scene.isEmpty then
      menu_button("new game"):
        new_game()
    else
      menu_button("resume game"):
        resume_game()
    menu_button("load game"):
      println("load game")
    menu_button("options"):
      println("options")
    menu_button("credits"):
      println("credits")
    if game_scene.nonEmpty then
      menu_button("quit game"):
        quit_game()
    menu_button("exit"):
      is_running = false

    menu

  def pretty_ui: ui.Element =
    val viewport = renderer.viewport

    val root = new ui.Float with ui.FixedWidth with ui.FixedHeight
    root.width = viewport.width
    root.height = viewport.height

    val menu = pretty_main_menu
    val menu_offset = new ui.Offset(menu) with ui.Center with ui.MaxWidth with ui.MaxHeight
    root.add_elem(menu_offset)

    val title = new ui.Label("Astrovoyageur", fonts("secondary"), 50.0, pretty_color) with ui.MinWidth with ui.MinHeight
    val title_offset = new ui.Offset(title) with ui.MaxWidth with ui.MaxHeight:
      override def offset(using ui.UiRenderer) =
        val menu_box = menu.abs_box - parent.get.abs_pos
        val x = abs_width / 2.0 - elem.abs_width / 2.0
        val space = abs_height - menu_box.top
        val min_y = menu_box.top + pretty_y_margin
        val y = (abs_height - space / 2.0 - elem.abs_height / 2.0) max min_y
        Vec2(x, y)
    root.add_elem(title_offset)

    val debug_info = new ui.List(ui.Direction.Down) with ui.MinWidth with ui.MinHeight
    root.add_elem(debug_info)

    @annotation.nowarn
    def debug_label(string: String, font: String = pretty_font) =
      val label = new ui.Label(string, fonts(font), 25.0, pretty_color) with ui.MinWidth with ui.MinHeight
      val result = new ui.Offset(label) with ui.Center with ui.MinWidth with ui.MinHeight with ui.FixedWidthMargin with ui.FixedHeightMargin
      result.width_margin = pretty_x_margin * 2.0
      result.height_margin = pretty_y_margin * 2.0
      debug_info.add_elem(result)

    root
