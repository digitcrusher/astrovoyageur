/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package astrovoyageur

import ludmotoro.game.renderers
import ludmotoro.game.physics.{Body, Circle}
import ludmotoro.graphics
import ludmotoro.graphics.{Colors, DrawShapes}
import ludmotoro.math.Vec2

class Ball(_pos: Vec2, val radius: Double) extends Body(Some(Circle(radius)), 0.6, _pos):
  override def draw(delta_time: Double) =
    for i <- scene.renderers[DrawShapes] do
      if aabb.exists(_.does_overlap_or_on_rect(i.viewport)) then
        i.draw(graphics.Circle(pos, radius).outline(Colors.green.copy(saturation = 0.5)))
