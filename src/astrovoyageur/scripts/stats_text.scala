/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package astrovoyageur.scripts

import ludmotoro.game.{InteractiveScript, renderers}
import ludmotoro.game.misc.create_phys_scene_stats
import ludmotoro.game.physics.PhysScene
import ludmotoro.graphics.{Button, Color, DrawText, Font, Renderer, Text, WindowLike, WindowState}
import ludmotoro.math.{Transforms2, Vec2}
import ludmotoro.utils.Stats

class StatsText(val stats_scene: PhysScene, val renderer: Renderer,
                var font: Font, var size: Double, var color: Color,
                var is_visible: Boolean = true)
    extends InteractiveScript:

  var stats: Stats = null

  override def on_start() =
    stats = create_phys_scene_stats(stats_scene, renderer)

  override def on_update(delta_time: Double) =
    stats.update_all("update")

  override def on_draw(delta_time: Double) =
    if is_visible then
      for i <- scene.renderers[DrawText] do
        val viewport = i.viewport
        val text = Text(stats.string, font, size, color)
        text.transform = Transforms2.move(Vec2(
          (viewport.left + viewport.right) / 2.0 - text.width / 2.0,
          viewport.top - text.top,
        ))
        i.draw(text)

    stats.update_all("draw")

  override def on_button_press(button: Button, past: WindowState, renderer: Renderer & WindowLike) =
    if button == Button.KeyH then
      is_visible = !is_visible
