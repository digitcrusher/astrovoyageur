/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package astrovoyageur.scripts

import ludmotoro.game.{InteractiveScript, renderers}
import ludmotoro.game.physics.Body
import ludmotoro.graphics.{Button, DrawShapes, Polygon, Renderer, Rgb, WindowLike, WindowState}

class AabbDrawer(var is_active: Boolean) extends InteractiveScript:
  override def on_draw(delta_time: Double) =
    if is_active then
      for renderer <- scene.renderers[DrawShapes] do
        val viewport = renderer.viewport
        for i <- scene.objects_of[Body] if i.ok do
          if i.aabb.exists(_.does_overlap_or_on_rect(viewport)) then
            renderer.draw(Polygon.from_rect(i.aabb.get).outline(Rgb(0.2)))

  override def on_button_press(button: Button, past: WindowState, renderer: Renderer & WindowLike) =
    if button == Button.KeyB then
      is_active = !is_active
