/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package astrovoyageur.scripts

import math.Pi

import ludmotoro.game.{InteractiveScript, renderers}
import ludmotoro.game.physics.Body
import ludmotoro.graphics.{Button, Renderer, WindowLike, WindowState}
import ludmotoro.math.Vec2

class Gravity(var uniform_acc: Double, var point_acc: Double,
              var point: Option[Vec2] = None, var is_active: Boolean = true)
    extends InteractiveScript:

  override def on_update(delta_time: Double) =
    for i <- scene.renderers[WindowLike] do
      if i.is_button_pressed(Button.KeyG) && i.is_button_pressed(Button.KeyLeftShift) then
        point = Some(i.untransform_pos(i.mouse_pos))

    if is_active then
      for i <- scene.objects_of[Body] if i.ok do
        if !i.is_static then
          if point.isEmpty then
            i.apply_impulse(Vec2.polar(uniform_acc * i.mass * delta_time, Pi * 1.5))
          else
            i.apply_impulse((point.get - i.pos).unit * point_acc * i.mass * delta_time)

  override def on_button_press(button: Button, past: WindowState, renderer: Renderer & WindowLike) =
    if button == Button.KeyG then
      if renderer.is_button_pressed(Button.KeyLeftCtrl) then
        point = None
      else if !renderer.is_button_pressed(Button.KeyLeftShift) then
        is_active = !is_active
