/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package astrovoyageur.scripts

import ludmotoro.game.Script
import ludmotoro.game.physics.Body

class TheVoid(var edge: Double) extends Script:
  override def on_update(delta_time: Double) =
    for i <- scene.objects_of[Body] if i.ok do
      if !i.is_static && i.pos.y < edge then
        i.delete()
