/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package astrovoyageur.scripts

import math.{log, Pi, pow}

import ludmotoro.game.InteractiveScript
import ludmotoro.game.physics.Body
import ludmotoro.graphics.{Button, Camera, Renderer, SpringAction, WindowLike, WindowState}
import ludmotoro.math.Vec2

// TODO: zoom onto cursor's position
class CameraControl(val camera: Camera, var gravity: Gravity) extends InteractiveScript:
  val camera_man = SpringAction()
  camera.add_action(camera_man)

  var tracked_body: Option[Body] = None
  var is_tracking_active = false

  override def on_update(delta_time: Double) =
    if is_tracking_active then
      tracked_body.foreach: body =>
        if body.ok then
          camera.pos = body.pos
          gravity.point.foreach: x =>
            camera_man.ori.target = x.angle_to(body.pos) - Pi / 2.0
        else
          tracked_body = None
          is_tracking_active = false

  override def on_button_press(button: Button, past: WindowState, renderer: Renderer & WindowLike) =
    button match
      case Button.MouseMiddle =>
        is_tracking_active = false
        camera_man.ori.target = 0.0

      case Button.KeyHome =>
        is_tracking_active = false
        camera.pos = Vec2.origin

      case Button.KeyT =>
        if past.is_button_pressed(Button.KeyLeftCtrl) then
          tracked_body = None
          is_tracking_active = false

        else if past.is_button_pressed(Button.KeyLeftShift) then
          if tracked_body.nonEmpty then
            is_tracking_active = true

        else
          val mouse_pos = renderer.untransform_pos(past.mouse_pos)
          var closest_dist = Double.PositiveInfinity
          for i <- scene.objects_of[Body] if i.ok do
            if !i.is_static then
              val dist = i.pos.sqr_dist_to(mouse_pos)
              if dist < closest_dist then
                closest_dist = dist
                tracked_body = Some(i)
                is_tracking_active = true

      case _ =>

  override def on_mouse_move(pos: Vec2, past: WindowState, renderer: Renderer & WindowLike) =
    if !is_tracking_active && past.is_button_pressed(Button.MouseMiddle) then
      val from = renderer.untransform_pos(past.mouse_pos)
      val to = renderer.untransform_pos(pos)
      camera.pos -= to - from

  override def on_mouse_scroll(scroll: Double, past: WindowState, renderer: Renderer & WindowLike) =
    val zoom_mul = 1.5
    val zoom_min = 0.1
    val zoom_max = 50.0
    val closest_min = pow(zoom_mul, (log(zoom_min) / log(zoom_mul)).ceil)
    val closest_max = pow(zoom_mul, (log(zoom_max) / log(zoom_mul)).floor)

    val zoom = camera_man.zoom.target * pow(zoom_mul, scroll) max closest_min min closest_max
    camera_man.zoom.target = zoom
