/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package astrovoyageur.scripts

import ludmotoro.game.{InteractiveScript, renderers}
import ludmotoro.graphics.{Button, Circle, Colors, DrawShapes, Renderer, WindowLike, WindowState}
import ludmotoro.math.Vec2

import astrovoyageur.Ball

class BallControl(ball_radius: => Double, var kill_radius: Double, var spawn_rate: Int = 1, var is_visible: Boolean = true)
    extends InteractiveScript:

  var grabbed: Option[Ball] = None
  var grabbed_last_pos = Vec2.origin
  var grab_renderer: Renderer & WindowLike = null

  override def on_update(delta_time: Double) =
    for renderer <- scene.renderers[WindowLike] do
      val mouse_pos = renderer.untransform_pos(renderer.mouse_pos)

      if renderer.is_button_pressed(Button.MouseRight) then
        for _ <- 1 to spawn_rate do
          scene.add_object(Ball(mouse_pos, ball_radius))

      if renderer.is_button_pressed(Button.MousePrev) then
        for i <- scene.objects_of[Ball] if i.ok do
          if i.pos.dist_to(mouse_pos) <= kill_radius then
            i.delete()

    grabbed.foreach: ball =>
      if ball.ok && grab_renderer.is_button_pressed(Button.MouseLeft) then
        val mouse_pos = grab_renderer.untransform_pos(grab_renderer.mouse_pos)
        ball.vel = (mouse_pos - grabbed_last_pos) / delta_time
        ball.pos = mouse_pos
        grabbed_last_pos = mouse_pos
        ball.is_static = true
      else
        ball.is_static = false
        grabbed = None

  override def on_draw(delta_time: Double) =
    if is_visible then
      for i <- scene.renderers[DrawShapes & WindowLike] do
        i.draw(Circle(i.untransform_pos(i.mouse_pos), kill_radius).outline(Colors.red.copy(saturation = 0.6)))

  override def on_button_press(button: Button, past: WindowState, renderer: Renderer & WindowLike) =
    button match
      case Button.KeyDelete =>
        for i <- scene.objects_of[Ball] if i.ok do
          i.delete()

      case Button.MouseLeft =>
        val mouse_pos = renderer.untransform_pos(past.mouse_pos)
        var closest_dist = Double.PositiveInfinity
        for i <- scene.objects_of[Ball] if i.ok do
          val dist = i.pos.sqr_dist_to(mouse_pos)
          if dist < closest_dist && dist <= i.radius * i.radius then
            closest_dist = dist
            grabbed = Some(i)
            grabbed_last_pos = mouse_pos
            grab_renderer = renderer

      case Button.KeyH =>
        is_visible = !is_visible

      case _ =>
