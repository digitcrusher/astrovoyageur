/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics

import collection.mutable

import ludmotoro.math.{Transform2, Transforms2}

class UploadedShape(var name: String, _transform: Transform2 = Transforms2.identity) extends Shape(_transform)

abstract trait DrawUploadedShape extends Renderer:
  override def dispatch_draw(shape: Shape) =
    shape match
      case x: UploadedShape => draw(x)
      case _ => super.dispatch_draw(shape)

  def draw(uploaded_shape: UploadedShape): Unit

abstract trait UploadedShapeData:
  def draw(shape: UploadedShape): Unit
  def unload(): Unit

trait Upload extends Renderer with DrawUploadedShape:
  override def draw(uploaded_shape: UploadedShape) =
    count_draw()

    if !uploaded_shape_datas.contains(uploaded_shape.name) then
      throw Exception(s"Unknown uploaded shape '${uploaded_shape.name}'")

    val data = uploaded_shape_datas(uploaded_shape.name)
    data.draw(uploaded_shape)

  var uploaded_shape_datas = mutable.Map[String, UploadedShapeData]()

  def upload(name: String, shape: Shape) =
    uploaded_shape_datas(name) = raw_upload(shape)

  def unload(name: String) =
    if !uploaded_shape_datas.contains(name) then
      throw Exception(s"Unknown uploaded shape '${name}'")

    uploaded_shape_datas(name).unload()
    uploaded_shape_datas -= name

  def raw_upload(shape: Shape): UploadedShapeData =
    throw Exception(s"Missing upload method for shape of ${shape.getClass}")

abstract trait UploadShapes extends Upload:
  override def raw_upload(shape: Shape) =
    shape match
      case x: ClearScreen => raw_upload(x)
      case x: Circle => raw_upload(x)
      case x: Ring => raw_upload(x)
      case x: Polygon => raw_upload(x)
      case _ => super.raw_upload(shape)

  def raw_upload(clear_screen: ClearScreen): UploadedShapeData
  def raw_upload(circle: Circle): UploadedShapeData
  def raw_upload(ring: Ring): UploadedShapeData
  def raw_upload(polygon: Polygon): UploadedShapeData
