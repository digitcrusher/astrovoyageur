/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics

import ludmotoro.math.{arc, arc_from_ends_and_radius, arc_from_limits, bezier, circle_from_radius,
                       circle_from_sidelen, ellipse, Rect, Side, Transform2, Transforms2, triangulate, Vec2}
import ludmotoro.utils.{circular_neighbors, circular_pairs}

class ClearScreen(var color: Color = Colors.black) extends Shape

class Circle(var pos: Vec2, var radius: Double,
             var color: Color = Colors.white,
             _transform: Transform2 = Transforms2.identity)
    extends Shape(_transform) with Outlineable[Ring]:

  def outline(color: Color, inner_offset: Double, outer_offset: Double) =
    Ring(pos, radius + inner_offset, radius + outer_offset, color, transform)

class Ring(var pos: Vec2, var inner_radius: Double, var outer_radius: Double,
           var color: Color = Colors.white,
           _transform: Transform2 = Transforms2.identity)
    extends Shape(_transform) with Outlineable[(Ring, Ring)]:

  def outline(color: Color, inner_offset: Double, outer_offset: Double) =
    (
      Ring(pos, inner_radius - outer_offset, inner_radius - inner_offset, color, transform),
      Ring(pos, outer_radius + inner_offset, outer_radius + outer_offset, color, transform),
    )

class Polygon(_points: IterableOnce[Vec2],
              var color: Color = Colors.white,
              _transform: Transform2 = Transforms2.identity)
    extends Shape(_transform) with Outlineable[Polygon]:

  val points =
    val temp = _points.iterator.toIndexedSeq
    if temp.circular_pairs.map(_ cross _).sum >= 0.0 then temp else temp.reverse

  val triangulation = triangulate(points)

  def outline(color: Color, inner_offset: Double, outer_offset: Double) =
    val result = Seq.newBuilder[Vec2]

    val corners = points.circular_neighbors.map(
      (a, b, c) =>
        val n1 = (b - a).rotated_by_270.unit
        val n2 = (c - b).rotated_by_270.unit
        (b, (n1 + n2) / (1 + (n1 dot n2)))
    ).toIndexedSeq

    for (pt, off) <- corners do
      result += pt + off * outer_offset
    result += corners.head._1 + corners.head._2 * outer_offset

    result += corners.head._1 + corners.head._2 * inner_offset
    for (pt, off) <- corners.reverseIterator do
      result += pt + off * inner_offset

    Polygon(result.result, color, transform)

object Polygon:
  def from_circle_radius(pos: Vec2, radius: Double, pointc: Int,
                         should_make_base_flat: Boolean = true,
                         color: Color = Colors.white) =

    Polygon(circle_from_radius(pos, radius, pointc, should_make_base_flat), color)

  def from_circle_sidelen(pos: Vec2, side_len: Double, pointc: Int,
                          should_make_base_flat: Boolean = true,
                          color: Color = Colors.white) =

    Polygon(circle_from_sidelen(pos, side_len, pointc, should_make_base_flat), color)

  def from_ellipse(pos: Vec2, radius_x: Double, radius_y: Double, pointc: Int,
                   should_make_base_flat: Boolean = true,
                   color: Color = Colors.white) =

    Polygon(ellipse(pos, radius_x, radius_y, pointc, should_make_base_flat), color)

  def from_rect(rect: Rect, color: Color = Colors.white) =
    Polygon(rect.vertices, color)

  def from_polyline(_points: IterableOnce[Vec2],
                    color: Color, left_width: Double, right_width: Double,
                    transform: Transform2): Polygon =

    val points = _points.iterator.toIndexedSeq

    if points.isEmpty then
      return Polygon(Nil, color, transform)

    val result = Seq.newBuilder[Vec2]

    val joints = points.sliding(3).map(
      x =>
        val n1 = (x(1) - x(0)).rotated_by_270.unit
        val n2 = (x(2) - x(1)).rotated_by_270.unit
        (x(1), (n1 + n2) / (1 + (n1 dot n2)))
    ).toIndexedSeq

    val n = points.size

    result += points(0) + (points(1) - points(0)).rotated_by_270.unit * right_width
    for (pt, off) <- joints do
      result += pt + off * right_width
    result += points(n - 1) + (points(n - 1) - points(n - 2)).rotated_by_270.unit * right_width

    result += points(n - 1) + (points(n - 2) - points(n - 1)).rotated_by_270.unit * left_width
    for (pt, off) <- joints.reverseIterator do
      result += pt - off * left_width
    result += points(0) + (points(0) - points(1)).rotated_by_270.unit * left_width

    Polygon(result.result, color, transform)

  def from_polyline(points: IterableOnce[Vec2],
                    color: Color = Colors.white, width: Double = 1.0,
                    transform: Transform2 = Transforms2.identity): Polygon =

    Polygon.from_polyline(points, color, width / 2.0, width / 2.0, transform)

  def from_arc(pos: Vec2, radius: Double, start_angle: Double, end_angle: Double, pointc: Int,
               color: Color = Colors.white, width: Double = 1.0) =

    Polygon.from_polyline(arc(pos, radius, start_angle, end_angle, pointc), color, width)

  def from_arc_ends_and_radius(pos1: Vec2, pos2: Vec2, radius: Double,
                               side: Side, pointc: Int,
                               should_draw_longer_side: Boolean = false,
                               color: Color = Colors.white, width: Double = 1.0) =

    Polygon.from_polyline(arc_from_ends_and_radius(pos1, pos2, radius, side, pointc, should_draw_longer_side), color, width)

  def from_arc_limits(pos: Vec2, limit1: Vec2, limit2: Vec2, pointc: Int,
                      color: Color = Colors.white, width: Double = 1.0) =

    Polygon.from_polyline(arc_from_limits(pos, limit1, limit2, pointc), color, width)

  def from_bezier(points: Iterable[Vec2], pointc: Int,
                  color: Color = Colors.white, width: Double = 1.0) =

    Polygon.from_polyline(bezier(points, pointc), color, width)

  def from_line(a: Vec2, b: Vec2,
                color: Color = Colors.white, width: Double = 1.0) =

    Polygon.from_polyline(Seq(a, b), color, width)

abstract trait DrawShapes extends Renderer:
  override def dispatch_draw(shape: Shape) =
    shape match
      case x: ClearScreen => draw(x)
      case x: Circle => draw(x)
      case x: Ring => draw(x)
      case x: Polygon => draw(x)
      case _ => super.dispatch_draw(shape)

  def draw(clear_screen: ClearScreen): Unit
  def draw(circle: Circle): Unit
  def draw(ring: Ring): Unit
  def draw(polygon: Polygon): Unit
