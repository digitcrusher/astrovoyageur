/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics

import collection.mutable

import ludmotoro.math.Vec2

// includes all IBM PC Model M standard US layout keyboard keys
enum Button:
  case MouseLeft, MouseMiddle, MouseRight, MousePrev, MouseNext

  case KeyEscape
  case KeyF1, KeyF2, KeyF3, KeyF4
  case KeyF5, KeyF6, KeyF7, KeyF8
  case KeyF9, KeyF10, KeyF11, KeyF12
  case KeyPrintScreen, KeyScrollLock, KeyPause
  case KeyGraveAccent, Key1, Key2, Key3, Key4, Key5, Key6, Key7, Key8, Key9, Key0, KeyMinus, KeyEquals, KeyBackspace
  case KeyTab, KeyQ, KeyW, KeyE, KeyR, KeyT, KeyY, KeyU, KeyI, KeyO, KeyP, KeyOpenBracket, KeyCloseBracket, KeyBackslash
  case KeyCapsLock, KeyA, KeyS, KeyD, KeyF, KeyG, KeyH, KeyJ, KeyK, KeyL, KeySemicolon, KeyApostrophe, KeyEnter
  case KeyLeftShift, KeyZ, KeyX, KeyC, KeyV, KeyB, KeyN, KeyM, KeyComma, KeyDot, KeySlash, KeyRightShift
  case KeyLeftCtrl, KeyLeftAlt, KeySpace, KeyRightAlt, KeyRightCtrl
  case KeyInsert, KeyDelete, KeyHome, KeyEnd, KeyPageUp, KeyPageDown
  case KeyUp, KeyLeft, KeyDown, KeyRight
  case KeyKpNumLock, KeyKpDivide, KeyKpSubtract, KeyKpMultiply, KeyKpAdd, KeyKpEnter
  case KeyKp7, KeyKp8, KeyKp9, KeyKp4, KeyKp5, KeyKp6, KeyKp1, KeyKp2, KeyKp3, KeyKp0, KeyKpDecimal

  case Unknown

// events should convey information about the new value and the old value
enum Event(var past: WindowState = null):
  case Close
  case Resize(size: Vec2)
  case Focus
  case Unfocus
  case ButtonPress(button: Button)
  case ButtonRelease(button: Button)
  case MouseMove(pos: Vec2)
  case MouseScroll(scroll: Double)
  case MouseEnter
  case MouseLeave
  case Char(codepoint: Int)

case class WindowState(should_close: Boolean,
                       size: Vec2,
                       is_focused: Boolean,
                       is_button_pressed: IndexedSeq[Boolean],
                       mouse_pos: Vec2,
                       is_mouse_in: Boolean):

  def is_button_pressed(button: Button): Boolean = is_button_pressed(button.ordinal)

  def updated(event: Event) =
    event match
      case Event.Close                 => copy(should_close = true)
      case Event.Resize(size)          => copy(size = size)
      case Event.Focus                 => copy(is_focused = true)
      case Event.Unfocus               => copy(is_focused = false)
      case Event.ButtonPress(button)   => copy(is_button_pressed = is_button_pressed.updated(button.ordinal, true))
      case Event.ButtonRelease(button) => copy(is_button_pressed = is_button_pressed.updated(button.ordinal, false))
      case Event.MouseMove(pos)        => copy(mouse_pos = pos)
      case Event.MouseScroll(scroll)   => this
      case Event.MouseEnter            => copy(is_mouse_in = true)
      case Event.MouseLeave            => copy(is_mouse_in = false)
      case Event.Char(codepoint)       => this

abstract trait WindowLike:
  var events: mutable.Queue[Event]
  var window_state: WindowState

  def should_close = window_state.should_close
  def size: Vec2 = window_state.size
  def width: Double = size.x
  def height: Double = size.y
  def is_focused = window_state.is_focused
  def is_button_pressed(button: Button) = window_state.is_button_pressed(button)
  def mouse_pos: Vec2 = window_state.mouse_pos
  def is_mouse_in = window_state.is_mouse_in
  def push_event(event: Event): Unit =
    event.past = window_state
    window_state = window_state.updated(event)
    events += event

abstract class Window extends WindowLike:
  def init(size: Vec2, title: String): Unit
  def delete(): Unit
  def refresh(): Unit

  var events = mutable.Queue[Event]()
  var window_state = WindowState(false, Vec2.zero, true, IndexedSeq.fill(Button.values.size)(false), Vec2.origin, false)
