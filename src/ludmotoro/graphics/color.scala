/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics

sealed abstract class Color:
  def to_rgb: Rgb
  def to_hsv: Hsv

case class Rgb(red: Double, green: Double, blue: Double, alpha: Double = 1.0) extends Color:
  def to_rgb = this
  def to_hsv =
    val min = red min green min blue
    val max = red max green max blue
    val delta = max - min
    var hue =
      if delta == 0.0 then
        0.0
      else if max == red then
        ((green - blue) / delta) * 60.0
      else if max == green then
        ((blue - red) / delta + 2.0) * 60.0
      else
        ((red - green) / delta + 4.0) * 60.0
    if hue < 0.0 then
      hue += 360.0
    val saturation = if max == 0.0 then 0.0 else delta / max
    Hsv(hue, saturation, max, alpha)

  def to_hex = (red   * 0xff).toInt << 16 |
               (green * 0xff).toInt << 8  |
               (blue  * 0xff).toInt << 0
  def to_hex_with_alpha = (red   * 0xff).toInt << 24 |
                          (green * 0xff).toInt << 16 |
                          (blue  * 0xff).toInt << 8  |
                          (alpha * 0xff).toInt << 0

  def inverse = Rgb(1.0 - red, 1.0 - green, 1.0 - blue, alpha)

object Rgb:
  def apply(value: Double): Rgb = Rgb(value, value, value)
  def apply(value: Double, alpha: Double): Rgb = Rgb(value, value, value, alpha)

  def from_hex(hex: Int) = Rgb(
    (hex >>> 16 & 0xff).toDouble / 0xff,
    (hex >>> 8 & 0xff).toDouble / 0xff,
    (hex >>> 0 & 0xff).toDouble / 0xff,
    1.0
  )
  def from_hex_with_alpha(hex: Int) = Rgb(
    (hex >>> 24 & 0xff).toDouble / 0xff,
    (hex >>> 16 & 0xff).toDouble / 0xff,
    (hex >>> 8 & 0xff).toDouble / 0xff,
    (hex >>> 0 & 0xff).toDouble / 0xff
  )

case class Hsv(hue: Double, saturation: Double, value: Double, alpha: Double = 1.0) extends Color:
  def to_hsv = this
  def to_rgb =
    val chroma = value * saturation
    val hue = this.hue / 60.0
    val x = chroma * (1.0 - (hue % 2.0 - 1.0).abs)
    val (red, green, blue) =
      if hue < 1.0 then (chroma, x, 0.0) else
      if hue < 2.0 then (x, chroma, 0.0) else
      if hue < 3.0 then (0.0, chroma, x) else
      if hue < 4.0 then (0.0, x, chroma) else
      if hue < 5.0 then (x, 0.0, chroma) else
                        (chroma, 0.0, x)
    val m = value - chroma
    Rgb(red + m, green + m, blue + m, alpha)

  def inverse = Hsv((hue + 180.0) % 360.0, 1.0 - saturation, 1.0 - value, alpha)
  def hue_inverse = Hsv((hue + 180.0) % 360.0, saturation, value, alpha)

object Hsv:
  def apply(hue: Double): Hsv = Hsv(hue, 1.0, 1.0)
  def apply(hue: Double, alpha: Double): Hsv = Hsv(hue, 1.0, 1.0, alpha)

object Colors:
  val transparent  = Rgb(1.0, 1.0, 1.0, 0.0)

  val white        = Rgb(1.0)
  val gray         = Rgb(0.5)
  val black        = Rgb(0.0)

  val red          = Hsv(0.0)
  val orange       = Hsv(30.0)
  val yellow       = Hsv(60.0)
  val chartreuse   = Hsv(90.0)
  val green        = Hsv(120.0)
  val spring_green = Hsv(150.0)
  val cyan         = Hsv(180.0)
  val azure        = Hsv(210.0)
  val blue         = Hsv(240.0)
  val violet       = Hsv(270.0)
  val magenta      = Hsv(300.0)
  val rose         = Hsv(330.0)

  val purple       = Hsv(270.0, 0.5, 0.5)
