/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics

import ludmotoro.math.{Rect, Transform2, Transforms2}
import ludmotoro.utils.lookahead

// TODO: formatted fonts

abstract class Font:
  def box_of(codepoint: Int, size: Double): Rect
  def max_box(size: Double): Rect
  def advance_width_of(codepoint: Int, size: Double, next_codepoint: Option[Int] = None): Double
  def advance_height(size: Double): Double

class Text(var string: String,
           var font: Font,
           var size: Double,
           var color: Color = Colors.white,
           _transform: Transform2 = Transforms2.identity)
    extends Shape(_transform):

  def left = if string.isEmpty then 0.0 else font.box_of(string.codePointAt(0), size).left
  def bottom = string.codePointStepper.iterator.map(font.box_of(_, size).bottom).minOption.getOrElse(0.0)
  def right = string.codePointStepper.iterator.lookahead.map(
      (a, b) => if b.isEmpty then font.box_of(a, size).right else font.advance_width_of(a, size, b)
    ).sum
  def top = string.codePointStepper.iterator.map(font.box_of(_, size).top).maxOption.getOrElse(0.0)

  def width = right - left
  def height = top - bottom

  def max_box =
    if string.isEmpty then
      Rect(0.0, 0.0, 0.0, 0.0)
    else
      val result = font.max_box(size)
      result.copy(right = result.right + result.width * (string.codePointCount(0, string.size) - 1))

  def advance_width = string.codePointStepper.iterator.lookahead.map(font.advance_width_of(_, size, _)).sum
  def advance_height = font.advance_height(size)

trait DrawText extends Renderer:
  override def dispatch_draw(shape: Shape) =
    shape match
      case x: Text => draw(x)
      case _ => super.dispatch_draw(shape)

  def draw(text: Text): Unit =
    throw Exception(s"Missing draw method for font of ${text.font.getClass}")
