/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics.ui

import collection.mutable
import util.boundary, boundary.break

import ludmotoro.graphics.{Color, Colors, DrawShapes, DrawText, Event, Font, Polygon, Renderer, Text, WindowLike}
import ludmotoro.math.{Rect, Transforms2, Vec2}

type UiRenderer = Renderer & DrawShapes & DrawText & WindowLike

abstract class Element:
  var parent: Option[Container] = None

  def width(using UiRenderer): Double // has no knowledge of the element's parent
  def height(using UiRenderer): Double // has no knowledge of the element's parent
  def abs_width(using UiRenderer): Double // knows the element's parent
  def abs_height(using UiRenderer): Double // knows the element's parent

  def abs_pos(using UiRenderer): Vec2 = // knows the element's parent
    parent.fold(Vec2.origin)(x => x.pos_of(this) + x.abs_pos)

  def abs_box(using UiRenderer) = // knows the element's parent
    Rect(0.0, 0.0, abs_width, abs_height) + abs_pos

  def is_pos_on_abs_this(pos: Vec2)(using UiRenderer): Boolean = // knows the element's parent
    pos.is_in_or_on_rect(abs_box) && parent.forall(_.is_pos_on_abs_this(pos))

  var debug_color = Colors.white
  def draw_debug_box()(using renderer: UiRenderer) =
    renderer.draw(Polygon.from_rect(abs_box).outline(debug_color))

  def draw()(using UiRenderer) = {}
  def on_event(event: Event)(using UiRenderer) = {}
  def handle_event(event: Event)(using renderer: UiRenderer) =
    val mouse_pos = renderer.untransform_pos(event.past.mouse_pos)
    if mouse_pos.is_in_or_on_rect(abs_box) then
      on_event(event)

abstract class Container extends Element:
  def elems: Iterable[Element]

  def pos_of(elem: Element)(using UiRenderer): Vec2 // knows the container's parent
  def max_width_of(elem: Element)(using UiRenderer): Double // knows the container's parent
  def max_height_of(elem: Element)(using UiRenderer): Double // knows the container's parent

  override def draw()(using UiRenderer) =
    for i <- elems do
      i.draw()

  override def on_event(event: Event)(using UiRenderer) =
    for i <- elems do
      i.handle_event(event)

trait MaxWidth extends Element:
  def abs_width(using UiRenderer) = parent.get.max_width_of(this)

trait MaxHeight extends Element:
  def abs_height(using UiRenderer) = parent.get.max_height_of(this)

trait MinWidth extends Element:
  def abs_width(using UiRenderer) = width

trait MinHeight extends Element:
  def abs_height(using UiRenderer) = height

trait FixedWidth extends Element:
  var width: Double = 0.0

  override def width(using UiRenderer) = width
  def abs_width(using UiRenderer) = width

trait FixedHeight extends Element:
  var height: Double = 0.0

  override def height(using UiRenderer) = height
  def abs_height(using UiRenderer) = height

abstract class Empty extends Element:
  def width(using UiRenderer) = 0.0
  def height(using UiRenderer) = 0.0

abstract class Offset(var _elem: Element) extends Container:
  def elem: Element = _elem
  def elem_=(value: Element) =
    elem.parent = None
    _elem = value
    elem.parent = Some(this)
  elem = _elem

  def width(using UiRenderer) = elem.width + width_margin
  def height(using UiRenderer) = elem.height + height_margin

  def elems = Some(elem)
  def pos_of(elem: Element)(using UiRenderer) = offset
  def max_width_of(elem: Element)(using UiRenderer) = abs_width
  def max_height_of(elem: Element)(using UiRenderer) = abs_height

  def offset(using UiRenderer) = Vec2.origin // knows the container's parent
  def width_margin(using UiRenderer) = 0.0 // has no knowledge of the container's parent
  def height_margin(using UiRenderer) = 0.0 // has no knowledge of the container's parent

trait Center extends Offset:
  override def offset(using UiRenderer) =
    val x = abs_width / 2.0 - elem.abs_width / 2.0
    val y = abs_height / 2.0 - elem.abs_height / 2.0
    Vec2(x, y)

trait FixedWidthMargin extends Offset:
  var width_margin = 0.0
  override def width_margin(using UiRenderer) = width_margin

trait FixedHeightMargin extends Offset:
  var height_margin = 0.0
  override def height_margin(using UiRenderer) = height_margin

abstract class Float extends Container:
  var _elems = mutable.Buffer[Element]()
  def elems = _elems

  def add_elem(elem: Element): Unit =
    elem.parent = Some(this)
    _elems += elem

  def add_elems(elems: Element*) =
    elems.foreach(add_elem)

  def width(using UiRenderer) = elems.map(x => pos_of(x).x + x.width).maxOption.getOrElse(0.0)
  def height(using UiRenderer) = elems.map(x => pos_of(x).y + x.height).maxOption.getOrElse(0.0)

  def pos_of(elem: Element)(using UiRenderer) = Vec2.origin
  def max_width_of(elem: Element)(using UiRenderer) = abs_width
  def max_height_of(elem: Element)(using UiRenderer) = abs_height

enum Direction:
  case Left, Right, Down, Up

abstract class List(var direction: Direction) extends Container:
  var _elems = mutable.Buffer[Element]()
  def elems = _elems

  def add_elem(elem: Element): Unit =
    elem.parent = Some(this)
    _elems += elem

  def add_elems(elems: Element*) =
    elems.foreach(add_elem)

  def width(using UiRenderer) =
    direction match
      case Direction.Left | Direction.Right => elems.map(_.width).sum
      case Direction.Down | Direction.Up => elems.map(_.width).maxOption.getOrElse(0.0)

  def height(using UiRenderer) =
    direction match
      case Direction.Left | Direction.Right => elems.map(_.height).maxOption.getOrElse(0.0)
      case Direction.Down | Direction.Up => elems.map(_.height).sum

  def pos_of(elem: Element)(using UiRenderer) =
    direction match
      case Direction.Left | Direction.Right =>
        var x = if direction == Direction.Left then abs_width else 0.0
        boundary:
          for i <- elems do
            if direction == Direction.Left then
              x -= i.abs_width
            if i == elem then
              break()
            if direction == Direction.Right then
              x += i.abs_width
        Vec2(x, 0.0)

      case Direction.Down | Direction.Up =>
        var y = if direction == Direction.Down then abs_height else 0.0
        boundary:
          for i <- elems do
            if direction == Direction.Down then
              y -= i.abs_height
            if i == elem then
              break()
            if direction == Direction.Up then
              y += i.abs_height
        Vec2(0.0, y)

  def max_width_of(elem: Element)(using UiRenderer) =
    direction match
      case Direction.Left | Direction.Right => abs_width - elems.withFilter(_ != elem).map(_.width).sum
      case Direction.Down | Direction.Up => abs_width

  def max_height_of(elem: Element)(using UiRenderer) =
    direction match
      case Direction.Left | Direction.Right => abs_height
      case Direction.Down | Direction.Up => abs_height - elems.withFilter(_ != elem).map(_.height).sum

abstract class Label(var string: String, var font: Font,
                     var size: Double, var color: Color = Colors.white,
                     var is_width_font_max: Boolean = false,
                     var is_height_font_max: Boolean = true)
    extends Element:

  def shape = Text(string, font, size, color)

  def width(using UiRenderer) = if is_width_font_max then shape.max_box.width else shape.width
  def height(using UiRenderer) = if is_height_font_max then shape.max_box.height else shape.height

  override def draw()(using renderer: UiRenderer) =
    val shape = this.shape
    val offset_x = if is_width_font_max then shape.max_box.left else shape.left
    val offset_y = if is_height_font_max then shape.max_box.bottom else shape.bottom
    val pos = abs_pos - Vec2(offset_x, offset_y)
    shape.transform = Transforms2.move(pos)
    renderer.draw(shape)
