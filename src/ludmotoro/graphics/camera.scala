/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics

import collection.mutable
import math.{Pi, pow, sqrt}

import ludmotoro.math.{AngleSpring, bezier_at, Spring, Transform2, Vec2, given}

abstract class Action(var duration: Option[Double] = None):
  var time = 0.0
  var camera: Camera = null

  var start_callback: Option[() => Unit] = None
  var stop_callback: Option[() => Unit] = None

  def apply(delta_time: Double): Unit

  def start() =
    start_callback.foreach:
      _()

  // IDEA: add cause
  def stop() =
    stop_callback.foreach:
      _()

  def is_done = duration.exists(time >= _)

class BaseAction(var pos: Option[Vec2] = None, var ori: Option[Double] = None,
                 var zoom: Option[Double] = None, _duration: Option[Double] = None)
    extends Action(_duration):

  override def apply(delta_time: Double) =
    pos.foreach:
      camera.pos = _
    ori.foreach:
      camera.ori = _
    zoom.foreach:
      camera.zoom = _

// HACK: calc velocity userstate
class PathAction(var path_func: Double => Vec2, _duration: Double) extends Action(Some(_duration)):
  override def apply(delta_time: Double) =
    camera.pos = path_func((time / duration.get) min 1.0)

// TODO: add polybezier
object Funcs2d:
  def linear(point1: Vec2, point2: Vec2)(t: Double) = point1 + (point2 - point1) * t
  def bezier(points: Iterable[Vec2])(t: Double) = bezier_at(t, points)

object Funcs1d:
  def linear(t: Double) = t

  def smooth_in_out(c: Double = 0.0)(t: Double) =
    val a = -2.0 + 2.0 * c
    val b = 3.0 - 3.0 * c
    a * pow(t, 3) + b * pow(t, 2) + c * t

  def smooth_in(t: Double) = t * t
  def smooth_out(t: Double) = -1.0 * pow(t, 2) + 2.0 * t

  // BUG: broken for x = 0.5
  def cubic_bezier(point: Vec2)(t: Double) =
    if point.x == 0.5 then
      throw Exception("x cannot be 0.5")

    val a = 2.0 * point.x - 1.0
    val b = 2.0 * point.y - 1.0
    val c = 2.0 * point.x - 2.0 * sqrt(pow(point.x, 2) - a * t)
    (b * t + c * point.y) / a - (b * c * point.x) / pow(a, 2)

class SpringAction(var pos: Spring[Vec2] = Spring[Vec2](0.0, 1.0, 1.0, false, Vec2.origin, Vec2.origin, Vec2.zero),
                   var ori: AngleSpring = AngleSpring(Pi * 50.0, 1.0, 1.0, true, 0.0, 0.0, 0.0),
                   var zoom: Spring[Double] = Spring[Double](100.0, 1.0, 1.0, true, 1.0, 0.0, 1.0),
                   _duration: Option[Double] = None)
    extends Action(_duration):

  override def apply(delta_time: Double) =
    if pos.is_active then
      pos.pos = camera.pos
      pos.vel = camera.user_state.getOrElse("lin_vel", Vec2.zero).asInstanceOf[Vec2]
      pos.step(delta_time)
      camera.pos = pos.pos
      camera.user_state("lin_vel") = pos.vel

    if ori.is_active then
      ori.pos = camera.ori
      ori.vel = camera.user_state.getOrElse("ang_vel", 0.0).asInstanceOf[Double]
      ori.step(delta_time)
      camera.ori = ori.pos
      camera.user_state("ang_vel") = ori.vel

    if zoom.is_active then
      zoom.pos = camera.zoom
      zoom.vel = camera.user_state.getOrElse("zoom_vel", 0.0).asInstanceOf[Double]
      zoom.step(delta_time)
      camera.zoom = zoom.pos
      camera.user_state("zoom_vel") = zoom.vel

class IntegrateAction(_duration: Option[Double] = None) extends Action(_duration):
  override def apply(delta_time: Double) =
    camera.pos += camera.user_state.getOrElse("lin_vel", Vec2.zero).asInstanceOf[Vec2] * delta_time
    camera.ori += camera.user_state.getOrElse("ang_vel", 0.0).asInstanceOf[Double] * delta_time
    camera.zoom += camera.user_state.getOrElse("zoom_vel", 0.0).asInstanceOf[Double] * delta_time

class Camera:
  var actions = mutable.IndexedBuffer[Action]()
  var user_state = mutable.Map[String, Any]()
  var is_paused = false
  var time = 0.0

  var pos = Vec2.origin
  var ori = 0.0
  var zoom = 1.0

  def update(delta_time: Double): Unit =
    time += delta_time

    if is_paused then return

    for i <- actions do
      i.time += delta_time
      i.apply(delta_time)
      if i.is_done then
        i.stop()
        i.camera = null
        actions -= i

  def transform =
    // operations:
    // - move by -pos
    // - rotate by -ori
    // - scale by zoom
    val sin = math.sin(-ori)
    val cos = math.cos(-ori)
    Transform2(cos, -sin, -pos.x * cos - pos.y * -sin,
               sin,  cos, -pos.x * sin - pos.y *  cos,
               0.0,  0.0,                  1.0 / zoom)

  def inverse_transform =
    // operations:
    // - scale by 1 / zoom
    // - rotate by ori
    // - move by pos
    val sin = math.sin(ori)
    val cos = math.cos(ori)
    Transform2(cos, -sin, pos.x * zoom,
               sin,  cos, pos.y * zoom,
               0.0,  0.0,         zoom)

  def add_action(action: Action) =
    actions += action
    action.camera = this
    action.start()

  def add_actions(actions: IterableOnce[Action]) =
    actions.iterator.foreach(add_action)

  def add_actions(actions: Action*): Unit =
    add_actions(actions)

  def insert_action(idx: Int, action: Action) =
    actions.insert(idx, action)
    action.camera = this
    action.start()

  def insert_actions(idx: Int, actions: IterableOnce[Action]) =
    this.actions.insertAll(idx, actions)
    for i <- actions.iterator do
      i.camera = this
      i.start()

  def set_actions(actions: IterableOnce[Action]) =
    stop_actions()
    add_actions(actions)

  def stop_actions() =
    for i <- actions do
      i.stop()
      i.camera = null
    actions.clear()
