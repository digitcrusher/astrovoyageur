/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics.lwjgl

import collection.mutable

import org.lwjgl.glfw.GLFW.*
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GL11.*

import ludmotoro.graphics.{Button, Event, Renderer, WindowLike, WindowState}
import ludmotoro.math.{Transform2, Vec2}

abstract class GlRenderer extends Renderer:
  override def begin() =
    glViewport(0, 0, width.toInt, height.toInt)
    glScissor(0, 0, width.toInt, height.toInt)

    glLoadIdentity()
    glScaled(1.0 / (width / 2.0), 1.0 / (height / 2.0), 1.0)

    super.begin()

  override def push_transform(transform: Transform2, inverse: Transform2) =
    super.push_transform(transform, inverse)

    glPushMatrix()
    glMultMatrixd(Array(
      transform.xx, transform.yx, 0.0, transform.zx,
      transform.xy, transform.yy, 0.0, transform.zy,
               0.0,          0.0, 1.0,          0.0,
      transform.xz, transform.yz, 0.0, transform.zz,
    ))

  override def pop_transform() =
    super.pop_transform()

    glPopMatrix()

  var gl_version = (1, 3)
  def request_min_gl_version(major: Int, minor: Int) =
    if major > gl_version._1 || (major == gl_version._1 && minor > gl_version._2) then
      gl_version = (major, minor)

open class LwjglRenderer(_size: Vec2, _title: String) extends GlRenderer with WindowLike:
  var window = GlfwWindow()

  def events = window.events
  def events_=(value: mutable.Queue[Event]) =
    window.events = value
  def window_state = window.window_state
  def window_state_=(value: WindowState) =
    window.window_state = value

  override def should_close = window.should_close
  override def size = window.size
  override def width = window.width
  override def height = window.height
  override def is_focused = window.is_focused
  override def is_button_pressed(button: Button) = window.is_button_pressed(button)
  override def mouse_pos = window.mouse_pos
  override def is_mouse_in = window.is_mouse_in
  override def push_event(event: Event) = window.push_event(event)

  override def init() =
    window.gl_version = gl_version
    window.init(_size, _title)

    glfwSwapInterval(0)
    glEnable(GL_SCISSOR_TEST)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    super.init()

  override def delete() =
    super.delete()

    window.delete()

  override def begin() =
    glfwMakeContextCurrent(window.glfw_window)
    GL.setCapabilities(window.gl_capabilities)

    super.begin()

  override def end() =
    super.end()

    window.refresh()
