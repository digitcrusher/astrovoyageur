/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics.lwjgl

import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL15.*
import org.lwjgl.opengl.GL20.*

import ludmotoro.graphics.{Circle, ClearScreen, Polygon, Rgb, Ring, UploadedShape, UploadedShapeData, UploadShapes}
import ludmotoro.math.Vec2

case class GlUploadedClearScreenData(color: Rgb) extends UploadedShapeData:
  def draw(shape: UploadedShape) =
    val Rgb(red, green, blue, alpha) = color
    if alpha == 1.0 then
      glClearColor(red.toFloat,  green.toFloat, blue.toFloat, alpha.toFloat)
      glClear(GL_COLOR_BUFFER_BIT)
    else
      glPushMatrix()
      glLoadIdentity()

      glColor4d(red, green, blue, alpha)
      glBegin(GL_QUADS)
        glVertex2i(1, 1)
        glVertex2i(-1, 1)
        glVertex2i(-1, -1)
        glVertex2i(1, -1)
      glEnd()

      glPopMatrix()

  def unload() = {}

case class GlUploadedCircleData(self: GlUploadShapes, vbo: Int, color: Rgb) extends UploadedShapeData:
  def draw(shape: UploadedShape) =
    glPushMatrix()
    glMultMatrixd(Array(
      shape.transform.xx, shape.transform.yx, 0.0, shape.transform.zx,
      shape.transform.xy, shape.transform.yy, 0.0, shape.transform.zy,
                     0.0,                0.0, 1.0,                0.0,
      shape.transform.xz, shape.transform.yz, 0.0, shape.transform.zz,
    ))

    val Rgb(red, green, blue, alpha) = color
    glColor4d(red, green, blue, alpha)

    self.circle_solid_shader.use:
      glEnableClientState(GL_VERTEX_ARRAY)
      glEnableClientState(GL_TEXTURE_COORD_ARRAY)

      glBindBuffer(GL_ARRAY_BUFFER, vbo)
      glTexCoordPointer(2, GL_DOUBLE, 8 * 4, 0)
      glVertexPointer(2, GL_DOUBLE, 8 * 4, 8 * 2)
      glDrawArrays(GL_QUADS, 0, 4)

      glDisableClientState(GL_TEXTURE_COORD_ARRAY)
      glDisableClientState(GL_VERTEX_ARRAY)

    glPopMatrix()

  def unload() =
    glDeleteBuffers(vbo)

case class GlUploadedRingData(self: GlUploadShapes, vbo: Int, inner_radius: Float, color: Rgb) extends UploadedShapeData:
  def draw(shape: UploadedShape) =
    glPushMatrix()
    glMultMatrixd(Array(
      shape.transform.xx, shape.transform.yx, 0.0, shape.transform.zx,
      shape.transform.xy, shape.transform.yy, 0.0, shape.transform.zy,
                     0.0,                0.0, 1.0,                0.0,
      shape.transform.xz, shape.transform.yz, 0.0, shape.transform.zz,
    ))

    val Rgb(red, green, blue, alpha) = color
    glColor4d(red, green, blue, alpha)

    self.ring_solid_shader.use:
      glUniform1f(self.ring_solid_shader.uniforms("inner_radius"), inner_radius)

      glEnableClientState(GL_VERTEX_ARRAY)
      glEnableClientState(GL_TEXTURE_COORD_ARRAY)

      glBindBuffer(GL_ARRAY_BUFFER, vbo)
      glTexCoordPointer(2, GL_DOUBLE, 8 * 4, 0)
      glVertexPointer(2, GL_DOUBLE, 8 * 4, 8 * 2)
      glDrawArrays(GL_QUADS, 0, 4)

      glDisableClientState(GL_TEXTURE_COORD_ARRAY)
      glDisableClientState(GL_VERTEX_ARRAY)

    glPopMatrix()

  def unload() =
    glDeleteBuffers(vbo)

case class GlUploadedPolygonData(vbo: Int, vertexc: Int, color: Rgb) extends UploadedShapeData:
  def draw(shape: UploadedShape) =
    glPushMatrix()
    glMultMatrixd(Array(
      shape.transform.xx, shape.transform.yx, 0.0, shape.transform.zx,
      shape.transform.xy, shape.transform.yy, 0.0, shape.transform.zy,
                     0.0,                0.0, 1.0,                0.0,
      shape.transform.xz, shape.transform.yz, 0.0, shape.transform.zz,
    ))

    val Rgb(red, green, blue, alpha) = color
    glColor4d(red, green, blue, alpha)

    glEnableClientState(GL_VERTEX_ARRAY)

    glBindBuffer(GL_ARRAY_BUFFER, vbo)
    glVertexPointer(2, GL_DOUBLE, 8 * 2, 0)
    glDrawArrays(GL_TRIANGLES, 0, vertexc)

    glDisableClientState(GL_VERTEX_ARRAY)

    glPopMatrix()

  def unload() =
    glDeleteBuffers(vbo)

trait GlUploadShapes extends GlRenderer with UploadShapes with GlDrawShapes:
  request_min_gl_version(2, 0)

  def raw_upload(clear_screen: ClearScreen) =
    GlUploadedClearScreenData(clear_screen.color.to_rgb)

  def raw_upload(circle: Circle) =
    val vbo = glGenBuffers()
    glBindBuffer(GL_ARRAY_BUFFER, vbo)

    val left = circle.pos.x - circle.radius
    val bottom = circle.pos.y - circle.radius
    val right = circle.pos.x + circle.radius
    val top = circle.pos.y + circle.radius

    val a = circle.transform * Vec2(left, bottom)
    val b = circle.transform * Vec2(right, bottom)
    val c = circle.transform * Vec2(right, top)
    val d = circle.transform * Vec2(left, top)

    glBufferData(GL_ARRAY_BUFFER, Array(
      0.0, 0.0, a.x, a.y,
      1.0, 0.0, b.x, b.y,
      1.0, 1.0, c.x, c.y,
      0.0, 1.0, d.x, d.y,
    ), GL_STATIC_DRAW)

    GlUploadedCircleData(this, vbo, circle.color.to_rgb)

  def raw_upload(ring: Ring) =
    val vbo = glGenBuffers()
    glBindBuffer(GL_ARRAY_BUFFER, vbo)

    val left = ring.pos.x - ring.outer_radius
    val bottom = ring.pos.y - ring.outer_radius
    val right = ring.pos.x + ring.outer_radius
    val top = ring.pos.y + ring.outer_radius

    val a = ring.transform * Vec2(left, bottom)
    val b = ring.transform * Vec2(right, bottom)
    val c = ring.transform * Vec2(right, top)
    val d = ring.transform * Vec2(left, top)

    glBufferData(GL_ARRAY_BUFFER, Array(
      0.0, 0.0, a.x, a.y,
      1.0, 0.0, b.x, b.y,
      1.0, 1.0, c.x, c.y,
      0.0, 1.0, d.x, d.y,
    ), GL_STATIC_DRAW)

    GlUploadedRingData(this, vbo, (ring.inner_radius / ring.outer_radius).toFloat, ring.color.to_rgb)

  def raw_upload(polygon: Polygon) =
    val vbo = glGenBuffers()
    glBindBuffer(GL_ARRAY_BUFFER, vbo)

    val vertices = Array.newBuilder[Double]
    inline def add(pts: Vec2*) =
      for i <- pts do
        val mapped = polygon.transform * i
        vertices += mapped.x
        vertices += mapped.y
    for (a, b, c) <- polygon.triangulation do
      add(a, b, c)
    glBufferData(GL_ARRAY_BUFFER, vertices.result, GL_STATIC_DRAW)

    GlUploadedPolygonData(vbo, 3 * polygon.points.size, polygon.color.to_rgb)
