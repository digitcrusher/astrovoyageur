/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics.lwjgl

import util.Using

import java.nio.channels.Channels

import org.lwjgl.BufferUtils.createByteBuffer
import org.lwjgl.opengl.GL11.*
import org.lwjgl.stb.STBTruetype.*
import org.lwjgl.stb.STBTTFontinfo
import org.lwjgl.system.MemoryStack

import ludmotoro.graphics.{DrawText, Font, Rgb, Text}
import ludmotoro.math.Rect

// obstacles i encountered when writing this module:
// - lwjgl segfaults when you pass wrapped buffers instead of direct ones
// - you need to set mag_filter/min_filter texture parameters before drawing a texture after binding it
// - opengl by default aligns texture rows to 4 bytes
// - a bitmap for a space character doesn't exist - stb returns null
// - xoff_ptr.get(0) is meant to be added to the current position, not substracted
// - You have to keep around a reference to the loaded file data because otherwise it's gonna be GC'ed

class StbFont(_filename: String, _font_name: String) extends Font:
  val (file, info) =
    val channel = Channels.newChannel(getClass.getClassLoader.getResourceAsStream(_filename))
    var buffer = createByteBuffer(1024)

    var should_continue_reading = true
    while should_continue_reading do
      if channel.read(buffer) == -1 then
        should_continue_reading = false
      else if buffer.remaining == 0 then
        val new_buffer = createByteBuffer(buffer.capacity * 3 / 2)
        buffer.flip()
        new_buffer.put(buffer)
        buffer = new_buffer
    buffer.flip()

    val result = STBTTFontinfo.create()

    val offset = stbtt_FindMatchingFont(buffer, _font_name, STBTT_MACSTYLE_DONTCARE)
    if offset == -1 then
      throw Exception(s"Failed to find font '${_font_name}' in '${_filename}'")
    if !stbtt_InitFont(result, buffer, offset) then
      throw Exception(s"Failed to load font '${_font_name}' from '${_filename}'")

    (buffer, result)

  def box_of(codepoint: Int, size: Double) =
    Using.resource(MemoryStack.stackPush()): stack =>
      val left_ptr = stack.mallocInt(1)
      val bottom_ptr = stack.mallocInt(1)
      val right_ptr = stack.mallocInt(1)
      val top_ptr = stack.mallocInt(1)
      stbtt_GetCodepointBox(info, codepoint, left_ptr, bottom_ptr, right_ptr, top_ptr)

      val scale = stbtt_ScaleForPixelHeight(info, size.toFloat)
      Rect(left_ptr.get(0), bottom_ptr.get(0), right_ptr.get(0), top_ptr.get(0)) * scale

  def max_box(size: Double) =
    Using.resource(MemoryStack.stackPush()): stack =>
      val left_ptr = stack.mallocInt(1)
      val bottom_ptr = stack.mallocInt(1)
      val right_ptr = stack.mallocInt(1)
      val top_ptr = stack.mallocInt(1)
      stbtt_GetFontBoundingBox(info, left_ptr, bottom_ptr, right_ptr, top_ptr)

      val scale = stbtt_ScaleForPixelHeight(info, size.toFloat)
      Rect(left_ptr.get(0), bottom_ptr.get(0), right_ptr.get(0), top_ptr.get(0)) * scale

  def advance_width_of(codepoint: Int, size: Double, next_codepoint: Option[Int] = None) =
    Using.resource(MemoryStack.stackPush()): stack =>
      val advance_width_ptr = stack.mallocInt(1)
      stbtt_GetCodepointHMetrics(info, codepoint, advance_width_ptr, null)

      val scale = stbtt_ScaleForPixelHeight(info, size.toFloat)

      var result = advance_width_ptr.get(0) * scale
      next_codepoint.foreach:
        result += stbtt_GetCodepointKernAdvance(info, codepoint, _) * scale
      result

  def advance_height(size: Double) =
    Using.resource(MemoryStack.stackPush()): stack =>
      val ascent_ptr = stack.mallocInt(1)
      val descent_ptr = stack.mallocInt(1)
      val line_gap_ptr = stack.mallocInt(1)
      stbtt_GetFontVMetrics(info, ascent_ptr, descent_ptr, line_gap_ptr)

      val scale = stbtt_ScaleForPixelHeight(info, size.toFloat)
      (ascent_ptr.get(0) - descent_ptr.get(0) + line_gap_ptr.get(0)) * scale

trait GlDrawText extends GlRenderer with DrawText:
  request_min_gl_version(2, 0)

  var glyph_tex = -1

  override def init_addons() =
    super.init_addons()
    glyph_tex = glGenTextures()

  override def draw(text: Text) =
    text.font match
      case x: StbFont => draw(text, x)
      case _ => super.draw(text)

  // IDEA: use subpixel funcs, correct height for transform
  def draw(text: Text, font: StbFont) =
    count_draw()

    push_transform(text.transform)

    glEnable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, glyph_tex)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
    val Rgb(red, green, blue, alpha) = text.color.to_rgb
    glColor4d(red, green, blue, alpha)

    val scale = stbtt_ScaleForPixelHeight(font.info, text.size.toFloat)

    Using.resource(MemoryStack.stackPush()): stack =>
      val width_ptr = stack.mallocInt(1)
      val height_ptr = stack.mallocInt(1)
      val xoff_ptr = stack.mallocInt(1)
      val yoff_ptr = stack.mallocInt(1)
      val advance_width_ptr = stack.mallocInt(1)

      var x = 0.0
      var last_codepoint = -1
      for codepoint <- text.string.codePoints.toArray do
        val kern_advance =
          if last_codepoint == -1 then
            0.0
          else
            stbtt_GetCodepointKernAdvance(font.info, last_codepoint, codepoint).toDouble * scale
        x += kern_advance

        val bitmap_scale = (scale / curr_transform.zz).toFloat
        val bitmap = stbtt_GetCodepointBitmap(font.info, bitmap_scale, bitmap_scale, codepoint,
                                              width_ptr, height_ptr, xoff_ptr, yoff_ptr)
        val bitmap_width = width_ptr.get(0)
        val bitmap_height = height_ptr.get(0)

        if bitmap_width > 0 && bitmap_height > 0 then
          glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, bitmap_width,
                       bitmap_height, 0, GL_ALPHA, GL_UNSIGNED_BYTE, bitmap)
          stbtt_FreeBitmap(bitmap)

          val width = bitmap_width * curr_transform.zz
          val height = bitmap_height * curr_transform.zz
          val xoff = xoff_ptr.get(0) * curr_transform.zz
          val yoff = yoff_ptr.get(0) * curr_transform.zz

          val left = x + xoff
          val bottom = -height - yoff
          val right = left + width
          val top = bottom + height

          glBegin(GL_QUADS)
            glTexCoord2i(1, 0); glVertex2d(right, top)
            glTexCoord2i(0, 0); glVertex2d(left, top)
            glTexCoord2i(0, 1); glVertex2d(left, bottom)
            glTexCoord2i(1, 1); glVertex2d(right, bottom)
          glEnd()

        stbtt_GetCodepointHMetrics(font.info, codepoint, advance_width_ptr, null)
        x += advance_width_ptr.get(0) * scale

        last_codepoint = codepoint

    glDisable(GL_TEXTURE_2D)

    pop_transform()
