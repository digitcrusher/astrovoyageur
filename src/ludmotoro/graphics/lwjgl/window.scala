/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics.lwjgl

import util.Using

import org.lwjgl.glfw.GLFW.*
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GLCapabilities
import org.lwjgl.system.MemoryUtil.NULL
import org.lwjgl.system.{MemoryStack, MemoryUtil}

import ludmotoro.graphics.{Button, Event, Window, WindowState}
import ludmotoro.math.Vec2

var was_glfw_inited = false
def init_glfw(): Unit =
  if was_glfw_inited then return

  glfwSetErrorCallback: (code: Int, msg: Long) =>
    println(s"Received glfw error [${code}]: '${MemoryUtil.memASCII(msg)}'")

  if !glfwInit() then
    throw Exception("Failed to initialize glfw")

  was_glfw_inited = true

def from_glfw_mouse(button: Int) =
  button match
    case GLFW_MOUSE_BUTTON_LEFT => Button.MouseLeft
    case GLFW_MOUSE_BUTTON_MIDDLE => Button.MouseMiddle
    case GLFW_MOUSE_BUTTON_RIGHT => Button.MouseRight
    case GLFW_MOUSE_BUTTON_4 => Button.MousePrev
    case GLFW_MOUSE_BUTTON_5 => Button.MouseNext

def from_glfw_key(key: Int) =
  key match
    case GLFW_KEY_ESCAPE => Button.KeyEscape

    case GLFW_KEY_F1 => Button.KeyF1
    case GLFW_KEY_F2 => Button.KeyF2
    case GLFW_KEY_F3 => Button.KeyF3
    case GLFW_KEY_F4 => Button.KeyF4

    case GLFW_KEY_F5 => Button.KeyF5
    case GLFW_KEY_F6 => Button.KeyF6
    case GLFW_KEY_F7 => Button.KeyF7
    case GLFW_KEY_F8 => Button.KeyF8

    case GLFW_KEY_F9 => Button.KeyF9
    case GLFW_KEY_F10 => Button.KeyF10
    case GLFW_KEY_F11 => Button.KeyF11
    case GLFW_KEY_F12 => Button.KeyF12

    case GLFW_KEY_PRINT_SCREEN => Button.KeyPrintScreen
    case GLFW_KEY_SCROLL_LOCK => Button.KeyScrollLock
    case GLFW_KEY_PAUSE => Button.KeyPause

    case GLFW_KEY_GRAVE_ACCENT => Button.KeyGraveAccent
    case GLFW_KEY_1 => Button.Key1
    case GLFW_KEY_2 => Button.Key2
    case GLFW_KEY_3 => Button.Key3
    case GLFW_KEY_4 => Button.Key4
    case GLFW_KEY_5 => Button.Key5
    case GLFW_KEY_6 => Button.Key6
    case GLFW_KEY_7 => Button.Key7
    case GLFW_KEY_8 => Button.Key8
    case GLFW_KEY_9 => Button.Key9
    case GLFW_KEY_0 => Button.Key0
    case GLFW_KEY_MINUS => Button.KeyMinus
    case GLFW_KEY_EQUAL => Button.KeyEquals
    case GLFW_KEY_BACKSPACE => Button.KeyBackspace

    case GLFW_KEY_TAB => Button.KeyTab
    case GLFW_KEY_Q => Button.KeyQ
    case GLFW_KEY_W => Button.KeyW
    case GLFW_KEY_E => Button.KeyE
    case GLFW_KEY_R => Button.KeyR
    case GLFW_KEY_T => Button.KeyT
    case GLFW_KEY_Y => Button.KeyY
    case GLFW_KEY_U => Button.KeyU
    case GLFW_KEY_I => Button.KeyI
    case GLFW_KEY_O => Button.KeyO
    case GLFW_KEY_P => Button.KeyP
    case GLFW_KEY_LEFT_BRACKET => Button.KeyOpenBracket
    case GLFW_KEY_RIGHT_BRACKET => Button.KeyCloseBracket
    case GLFW_KEY_BACKSLASH => Button.KeyBackslash

    case GLFW_KEY_CAPS_LOCK => Button.KeyCapsLock
    case GLFW_KEY_A => Button.KeyA
    case GLFW_KEY_S => Button.KeyS
    case GLFW_KEY_D => Button.KeyD
    case GLFW_KEY_F => Button.KeyF
    case GLFW_KEY_G => Button.KeyG
    case GLFW_KEY_H => Button.KeyH
    case GLFW_KEY_J => Button.KeyJ
    case GLFW_KEY_K => Button.KeyK
    case GLFW_KEY_L => Button.KeyL
    case GLFW_KEY_SEMICOLON => Button.KeySemicolon
    case GLFW_KEY_APOSTROPHE => Button.KeyApostrophe
    case GLFW_KEY_ENTER => Button.KeyEnter

    case GLFW_KEY_LEFT_SHIFT => Button.KeyLeftShift
    case GLFW_KEY_Z => Button.KeyZ
    case GLFW_KEY_X => Button.KeyX
    case GLFW_KEY_C => Button.KeyC
    case GLFW_KEY_V => Button.KeyV
    case GLFW_KEY_B => Button.KeyB
    case GLFW_KEY_N => Button.KeyN
    case GLFW_KEY_M => Button.KeyM
    case GLFW_KEY_COMMA => Button.KeyComma
    case GLFW_KEY_PERIOD => Button.KeyDot
    case GLFW_KEY_SLASH => Button.KeySlash
    case GLFW_KEY_RIGHT_SHIFT => Button.KeyRightShift

    case GLFW_KEY_LEFT_CONTROL => Button.KeyLeftCtrl
    case GLFW_KEY_LEFT_ALT => Button.KeyLeftAlt
    case GLFW_KEY_SPACE => Button.KeySpace
    case GLFW_KEY_RIGHT_ALT => Button.KeyRightAlt
    case GLFW_KEY_RIGHT_CONTROL => Button.KeyRightCtrl

    case GLFW_KEY_INSERT => Button.KeyInsert
    case GLFW_KEY_DELETE => Button.KeyDelete
    case GLFW_KEY_HOME => Button.KeyHome
    case GLFW_KEY_END => Button.KeyEnd
    case GLFW_KEY_PAGE_UP => Button.KeyPageUp
    case GLFW_KEY_PAGE_DOWN => Button.KeyPageDown

    case GLFW_KEY_UP => Button.KeyUp
    case GLFW_KEY_LEFT => Button.KeyLeft
    case GLFW_KEY_DOWN => Button.KeyDown
    case GLFW_KEY_RIGHT => Button.KeyRight

    case GLFW_KEY_NUM_LOCK => Button.KeyKpNumLock
    case GLFW_KEY_KP_DIVIDE => Button.KeyKpDivide
    case GLFW_KEY_KP_SUBTRACT => Button.KeyKpSubtract
    case GLFW_KEY_KP_MULTIPLY => Button.KeyKpMultiply
    case GLFW_KEY_KP_ADD => Button.KeyKpAdd
    case GLFW_KEY_KP_ENTER => Button.KeyKpEnter

    case GLFW_KEY_KP_7 => Button.KeyKp7
    case GLFW_KEY_KP_8 => Button.KeyKp8
    case GLFW_KEY_KP_9 => Button.KeyKp9
    case GLFW_KEY_KP_4 => Button.KeyKp4
    case GLFW_KEY_KP_5 => Button.KeyKp5
    case GLFW_KEY_KP_6 => Button.KeyKp6
    case GLFW_KEY_KP_1 => Button.KeyKp1
    case GLFW_KEY_KP_2 => Button.KeyKp2
    case GLFW_KEY_KP_3 => Button.KeyKp3
    case GLFW_KEY_KP_0 => Button.KeyKp0
    case GLFW_KEY_KP_DECIMAL => Button.KeyKpDecimal

    case _ =>
      println(s"Unknown GLFW key ${key}")
      Button.Unknown

class GlfwWindow extends Window:
  var gl_version = (1, 0)

  var glfw_window: Long = -1
  var gl_capabilities: GLCapabilities = null

  def init(size: Vec2, title: String) =
    init_glfw()

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, gl_version._1)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, gl_version._2)
    glfwWindowHint(GLFW_SAMPLES, 4)
    glfw_window = glfwCreateWindow(size.x.toInt, size.y.toInt, title, NULL, NULL)

    glfwSetWindowCloseCallback(glfw_window, (window: Long) =>
      push_event(Event.Close)
    )

    glfwSetFramebufferSizeCallback(glfw_window, (window: Long, width: Int, height: Int) =>
      push_event(Event.Resize(Vec2(width.toDouble, height.toDouble)))
    )

    glfwSetWindowFocusCallback(glfw_window, (window: Long, was_focused: Boolean) =>
      if was_focused then
        push_event(Event.Focus)
      else
        push_event(Event.Unfocus)
    )

    glfwSetMouseButtonCallback(glfw_window, (window: Long, button: Int, action: Int, modifiers: Int) =>
      if action == GLFW_PRESS then
        push_event(Event.ButtonPress(from_glfw_mouse(button)))
      else if action == GLFW_RELEASE then
        push_event(Event.ButtonRelease(from_glfw_mouse(button)))
    )

    glfwSetKeyCallback(glfw_window, (window: Long, key: Int, scancode: Int, action: Int, modifiers: Int) =>
      if action == GLFW_PRESS then
        push_event(Event.ButtonPress(from_glfw_key(key)))
      else if action == GLFW_RELEASE then
        push_event(Event.ButtonRelease(from_glfw_key(key)))
    )

    glfwSetCursorPosCallback(glfw_window, (window: Long, x: Double, y: Double) =>
      push_event(Event.MouseMove(glfw_pos_to_our_pos(Vec2(x, y))))
    )

    glfwSetScrollCallback(glfw_window, (window: Long, x: Double, y: Double) =>
      push_event(Event.MouseScroll(y))
    )

    glfwSetCursorEnterCallback(glfw_window, (window: Long, has_entered: Boolean) =>
      if has_entered then
        push_event(Event.MouseEnter)
      else
        push_event(Event.MouseLeave)
        push_event(Event.MouseMove(glfw_pos_to_our_pos(glfw_mouse_pos)))
    )

    glfwSetCharCallback(glfw_window, (window: Long, codepoint: Int) =>
      push_event(Event.Char(codepoint))
    )

    window_state = WindowState(false, size, true, IndexedSeq.fill(Button.values.size)(false),
                               glfw_pos_to_our_pos(glfw_mouse_pos), glfwGetWindowAttrib(glfw_window, GLFW_HOVERED) != 0)

    glfwMakeContextCurrent(glfw_window)
    gl_capabilities = GL.createCapabilities()

  def delete() =
    glfwDestroyWindow(glfw_window)

  def refresh() =
    glfwSwapBuffers(glfw_window)
    glfwPollEvents()

  def glfw_mouse_pos =
    Using.resource(MemoryStack.stackPush()): stack =>
      val x_ptr = stack.mallocDouble(1)
      val y_ptr = stack.mallocDouble(1)
      glfwGetCursorPos(glfw_window, x_ptr, y_ptr)

      Vec2(x_ptr.get(0) max 0.0 min size.x,
           y_ptr.get(0) max 0.0 min size.y)

  def glfw_pos_to_our_pos(pos: Vec2) = (pos - Vec2(size.x / 2.0, size.y / 2.0)) * Vec2(1.0, -1.0)
