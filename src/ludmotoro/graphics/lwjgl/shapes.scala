/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics.lwjgl

import io.Source
import util.Using

import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL20.*
import org.lwjgl.system.MemoryStack

import ludmotoro.graphics.{Circle, ClearScreen, DrawShapes, Polygon, Rgb, Ring}
import ludmotoro.math.Transforms2

class Shader(_vert_resource: String, _frag_resource: String):
  val id =
    var vert = 0
    var frag = 0
    try
      inline def create_shader_stage(_type: Int, resource: String) =
        val result = glCreateShader(_type)
        glShaderSource(result, Source.fromResource(resource).mkString)
        glCompileShader(result)

        if glGetShaderi(result, GL_COMPILE_STATUS) == GL_FALSE then
          val log = glGetShaderInfoLog(result)
          glDeleteShader(result)
          throw Exception(s"Failed to compile shader stage:\n${log}")

        result

      vert = create_shader_stage(GL_VERTEX_SHADER, _vert_resource)
      frag = create_shader_stage(GL_FRAGMENT_SHADER, _frag_resource)

      val result = glCreateProgram()
      glAttachShader(result, vert)
      glAttachShader(result, frag)
      glLinkProgram(result)

      if glGetProgrami(result, GL_LINK_STATUS) == GL_FALSE then
        val log = glGetProgramInfoLog(result)
        glDeleteProgram(result)
        throw Exception(s"Failed to link shader:\n${log}")

      result

    finally
      glDeleteShader(vert)
      glDeleteShader(frag)

  val uniforms =
    Using.resource(MemoryStack.stackPush()): stack =>
      val dummy = stack.mallocInt(1)
      Map.from(
        for i <- 0 until glGetProgrami(id, GL_ACTIVE_UNIFORMS) yield
          val name = glGetActiveUniform(id, i, dummy, dummy)
          name -> glGetUniformLocation(id, name)
      )

  def use(body: => Unit) =
    glUseProgram(id)
    body
    glUseProgram(0)

trait GlDrawShapes extends GlRenderer with DrawShapes:
  request_min_gl_version(2, 0)

  var circle_solid_shader: Shader = null
  var ring_solid_shader: Shader = null

  override def init_addons() =
    super.init_addons()

    circle_solid_shader = Shader("res/circle_solid.vert", "res/circle_solid.frag")
    ring_solid_shader = Shader("res/ring_solid.vert", "res/ring_solid.frag")

  def draw(clear_screen: ClearScreen) =
    count_draw()

    val Rgb(red, green, blue, alpha) = clear_screen.color.to_rgb
    if alpha == 1.0 then
      glClearColor(red.toFloat, green.toFloat, blue.toFloat, alpha.toFloat)
      glClear(GL_COLOR_BUFFER_BIT)
    else
      glPushMatrix()
      glLoadIdentity()

      glColor4d(red, green, blue, alpha)
      glBegin(GL_QUADS)
        glVertex2i(1, 1)
        glVertex2i(-1, 1)
        glVertex2i(-1, -1)
        glVertex2i(1, -1)
      glEnd()

      glPopMatrix()

  def draw(circle: Circle) =
    count_draw()

    if circle.transform != Transforms2.identity then
      glPushMatrix()
      glMultMatrixd(Array(
        circle.transform.xx, circle.transform.yx, 0.0, circle.transform.zx,
        circle.transform.xy, circle.transform.yy, 0.0, circle.transform.zy,
                        0.0,                 0.0, 1.0,                 0.0,
        circle.transform.xz, circle.transform.yz, 0.0, circle.transform.zz,
      ))

    val Rgb(red, green, blue, alpha) = circle.color.to_rgb
    glColor4d(red, green, blue, alpha)

    circle_solid_shader.use:
      val left = circle.pos.x - circle.radius
      val bottom = circle.pos.y - circle.radius
      val right = circle.pos.x + circle.radius
      val top = circle.pos.y + circle.radius
      glBegin(GL_QUADS)
        glTexCoord2i(0, 0); glVertex2d(left, bottom)
        glTexCoord2i(1, 0); glVertex2d(right, bottom)
        glTexCoord2i(1, 1); glVertex2d(right, top)
        glTexCoord2i(0, 1); glVertex2d(left, top)
      glEnd()

    if circle.transform != Transforms2.identity then
      glPopMatrix()

  def draw(ring: Ring) =
    count_draw()

    if ring.transform != Transforms2.identity then
      glPushMatrix()
      glMultMatrixd(Array(
        ring.transform.xx, ring.transform.yx, 0.0, ring.transform.zx,
        ring.transform.xy, ring.transform.yy, 0.0, ring.transform.zy,
                      0.0,               0.0, 1.0,               0.0,
        ring.transform.xz, ring.transform.yz, 0.0, ring.transform.zz,
      ))

    val Rgb(red, green, blue, alpha) = ring.color.to_rgb
    glColor4d(red, green, blue, alpha)

    ring_solid_shader.use:
      glUniform1f(ring_solid_shader.uniforms("inner_radius"), (ring.inner_radius / ring.outer_radius).toFloat)

      val left = ring.pos.x - ring.outer_radius
      val bottom = ring.pos.y - ring.outer_radius
      val right = ring.pos.x + ring.outer_radius
      val top = ring.pos.y + ring.outer_radius
      glBegin(GL_QUADS)
        glTexCoord2i(0, 0); glVertex2d(left, bottom)
        glTexCoord2i(1, 0); glVertex2d(right, bottom)
        glTexCoord2i(1, 1); glVertex2d(right, top)
        glTexCoord2i(0, 1); glVertex2d(left, top)
      glEnd()

    if ring.transform != Transforms2.identity then
      glPopMatrix()

  def draw(polygon: Polygon) =
    count_draw()

    glPushMatrix()
    glMultMatrixd(Array(
      polygon.transform.xx, polygon.transform.yx, 0.0, polygon.transform.zx,
      polygon.transform.xy, polygon.transform.yy, 0.0, polygon.transform.zy,
                       0.0,                  0.0, 1.0,                  0.0,
      polygon.transform.xz, polygon.transform.yz, 0.0, polygon.transform.zz,
    ))

    val Rgb(red, green, blue, alpha) = polygon.color.to_rgb
    glColor4d(red, green, blue, alpha)
    glBegin(GL_TRIANGLES)
    for (a, b, c) <- polygon.triangulation do
      glVertex2d(a.x, a.y)
      glVertex2d(b.x, b.y)
      glVertex2d(c.x, c.y)
    glEnd()

    glPopMatrix()
