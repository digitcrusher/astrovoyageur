/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.graphics

import collection.mutable

import ludmotoro.math.{Rect, Transform2, Transforms2, Vec2}

// TODO: add textures
// IDEA: off-screen texture renderer targets
// IDEA: constructive solid geometry
// IDEA: vector image drawing (SVG)

abstract class Shape(var transform: Transform2 = Transforms2.identity)

enum Outline:
  case Inside, Middle, Outside

abstract trait Outlineable[A]:
  def outline(color: Color, inner_offset: Double, outer_offset: Double): A

  def outline(color: Color = Colors.white, width: Double = 1.0, placement: Outline = Outline.Inside): A =
    placement match
      case Outline.Inside => outline(color, -width, 0.0)
      case Outline.Middle => outline(color, -width / 2.0, width / 2.0)
      case Outline.Outside => outline(color, 0.0, width)

abstract class Renderer:
  def init() =
    init_addons()

  def delete() = {}

  def begin() =
    drawc = 0
    dispatchc = 0
    transform_stack.clear()
    _viewport = None

  def end() = {}

  var drawc = 0
  var dispatchc = 0

  def draw(shape: Shape) =
    dispatchc += 1
    dispatch_draw(shape)

  def draw(shapes: IterableOnce[Shape]): Unit =
    shapes.iterator.foreach(draw)

  def draw(shapes: Shape*): Unit =
    draw(shapes)

  def init_addons() = {}

  def dispatch_draw(shape: Shape): Unit =
    throw Exception(s"Missing draw method for shape of ${shape.getClass}")

  def count_draw() =
    drawc += 1

  val transform_stack = mutable.Stack[(Transform2, Transform2)]()

  def curr_transform = if transform_stack.isEmpty then Transforms2.identity else transform_stack.top._1
  def curr_inverse_transform = if transform_stack.isEmpty then Transforms2.identity else transform_stack.top._2

  def transform_pos(pos: Vec2) = curr_transform * pos
  def untransform_pos(pos: Vec2) = curr_inverse_transform * pos

  def push_transform(transform: Transform2, inverse: Transform2): Unit =
    if transform_stack.isEmpty then
      transform_stack.push((transform, inverse))
    else
      transform_stack.push((transform_stack.top._1 * transform, inverse * transform_stack.top._2))
    _viewport = None

  def push_transform(transform: Transform2): Unit =
    push_transform(transform, transform.inverse)

  def pop_transform(): Unit =
    transform_stack.pop(): @annotation.nowarn
    _viewport = None

  def size: Vec2
  def width = size.x
  def height = size.y
  var _viewport: Option[Rect] = None
  def viewport =
    if _viewport.isEmpty then
      val w = width / 2.0
      val h = height / 2.0

      val tr = untransform_pos(Vec2(w, h))
      val tl = untransform_pos(Vec2(-w, h))
      val bl = untransform_pos(Vec2(-w, -h))
      val br = untransform_pos(Vec2(w, -h))

      val min_x = tr.x min tl.x min bl.x min br.x
      val min_y = tr.y min tl.y min bl.y min br.y
      val max_x = tr.x max tl.x max bl.x max br.x
      val max_y = tr.y max tl.y max bl.y max br.y
      _viewport = Some(Rect(min_x, min_y, max_x, max_y))

    _viewport.get
