/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.math

import collection.mutable

// C * B * A * v will result in v being first transformed by A, then B and then C
class Transform2:
  var array = mutable.IndexedSeq.fill(3, 3)(0.0)

  def this(elems: Double*) =
    this()
    if elems.size != 9 then
      throw Exception("Invalid number of elements passed, you might have forgotten to pass the size")
    for i <- 0 until 3
        j <- 0 until 3 do
      this(i, j) = elems(3 * i + j)

  def xx = this(0, 0)
  def xy = this(0, 1)
  def xz = this(0, 2)
  def yx = this(1, 0)
  def yy = this(1, 1)
  def yz = this(1, 2)
  def zx = this(2, 0)
  def zy = this(2, 1)
  def zz = this(2, 2)

  def xx_=(value: Double) = this(0, 0) = value
  def xy_=(value: Double) = this(0, 1) = value
  def xz_=(value: Double) = this(0, 2) = value
  def yx_=(value: Double) = this(1, 0) = value
  def yy_=(value: Double) = this(1, 1) = value
  def yz_=(value: Double) = this(1, 2) = value
  def zx_=(value: Double) = this(2, 0) = value
  def zy_=(value: Double) = this(2, 1) = value
  def zz_=(value: Double) = this(2, 2) = value

  def apply(row: Int, col: Int) = array(row)(col)
  def update(row: Int, col: Int, value: Double) =
    array(row)(col) = value

  def as_row_mayor = array.flatten
  def as_column_mayor = array.transpose.flatten

  override def toString = s"Transform2(${as_row_mayor.mkString(", ")})"

  def ==(other: Transform2) = array == other.array
  def !=(other: Transform2) = !(this == other)

  def +(other: Transform2) =
    val result = Transform2()
    for i <- 0 until 3
        j <- 0 until 3 do
      result(i, j) = this(i, j) + other(i, j)
    result

  def -(other: Transform2) =
    val result = Transform2()
    for i <- 0 until 3
        j <- 0 until 3 do
      result(i, j) = this(i, j) - other(i, j)
    result

  def *(other: Transform2) =
    val result = Transform2()
    for i <- 0 until 3
        j <- 0 until 3
        k <- 0 until 3 do
      result(i, j) += this(i, k) * other(k, j)
    result

  def *(scalar: Double) =
    val result = Transform2()
    for i <- 0 until 3
        j <- 0 until 3 do
      result(i, j) = this(i, j) * scalar
    result

  def /(scalar: Double) =
    val result = Transform2()
    for i <- 0 until 3
        j <- 0 until 3 do
      result(i, j) = this(i, j) / scalar
    result

  def *(vec: Vec2) =
    val x = xx * vec.x + xy * vec.y + xz * 1.0
    val y = yx * vec.x + yy * vec.y + yz * 1.0
    val z = zx * vec.x + zy * vec.y + zz * 1.0
    Vec2(x / z, y / z)

  def abs =
    val result = Transform2()
    for i <- 0 until 3
        j <- 0 until 3 do
      result(i, j) = this(i, j).abs
    result

  def unary_- =
    val result = Transform2()
    for i <- 0 until 3
        j <- 0 until 3 do
      result(i, j) = -this(i, j)
    result

  def transposed =
    val result = Transform2()
    for i <- 0 until 3
        j <- 0 until 3 do
      result(i, j) = this(j, i)
    result

  def minor(row: Int, col: Int) =
    import language.implicitConversions
    given Conversion[Boolean, Int] = if _ then 1 else 0

    val xx = this(row == 0, col == 0)
    val xy = this(row == 0, 2 - (col == 2))
    val yx = this(2 - (row == 2), col == 0)
    val yy = this(2 - (row == 2), 2 - (col == 2))
    xx * yy - xy * yx

  def determinant =
    if zx == 0.0 && zy == 0.0 && zz == 1.0 then
      xx * yy - xy * yx
    else if zx == 0.0 && zy == 0.0 then
      (xx * yy - xy * yx) * zz
    else
      xx * minor(0, 0) - xy * minor(0, 1) + xz * minor(0, 2)

  def inverse =
    val result = Transform2()
    val trans = transposed
    val det = determinant
    for i <- 0 until 3
        j <- 0 until 3 do
      result(i, j) = trans.minor(i, j) / det * (if (i + j) % 2 == 0 then 1.0 else -1.0)
    result

object Transforms2:
  val identity = Transform2(1.0, 0.0, 0.0,
                            0.0, 1.0, 0.0,
                            0.0, 0.0, 1.0)

  def move(offset: Vec2) = Transform2(1.0, 0.0, offset.x,
                                      0.0, 1.0, offset.y,
                                      0.0, 0.0,      1.0)

  def scale(factor: Vec2) = Transform2(factor.x,      0.0, 0.0,
                                            0.0, factor.y, 0.0,
                                            0.0,      0.0, 1.0)

  def scale(factor: Double) = Transform2(factor,    0.0, 0.0,
                                            0.0, factor, 0.0,
                                            0.0,    0.0, 1.0)

  def rotate(angle: Double) =
    val sin = math.sin(angle)
    val cos = math.cos(angle)
    Transform2(cos, -sin, 0.0,
               sin,  cos, 0.0,
               0.0,  0.0, 1.0)
