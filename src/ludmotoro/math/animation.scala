/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.math

import math.{cos, exp, Pi, pow, sin, sqrt}

import ludmotoro.utils.{mod, given}

abstract trait Ops[A]:
  def +(a: A, b: A): A
  def -(a: A, b: A): A
  def *(a: A, b: Double): A
  def /(a: A, b: Double): A

extension [A](a: A)(using ops: Ops[A])
  def +(b: A) = ops.+(a, b)
  def -(b: A) = ops.-(a, b)
  def *(b: Double) = ops.*(a, b)
  def /(b: Double) = ops./(a, b)

given Ops[Double] with
  def +(a: Double, b: Double) = a + b
  def -(a: Double, b: Double) = a - b
  def *(a: Double, b: Double) = a * b
  def /(a: Double, b: Double) = a / b

given Ops[Vec2] with
  def +(a: Vec2, b: Vec2) = a + b
  def -(a: Vec2, b: Vec2) = a - b
  def *(a: Vec2, b: Double) = a * b
  def /(a: Vec2, b: Double) = a / b

// A has to support A + A, A - A, A * Double scalar, A / Double scalar
class Spring[A](var coef: Double, var damping_ratio: Double,
                var mass: Double, var is_active: Boolean,
                var target: A, var pos: A, var vel: A):

  def displacement(using Ops[A]) = target - pos

  def predict(delta_time: Double)(using Ops[A]): (A, A) =
    if mass <= 0.0 then
      throw Exception("A spring's mass must be positive")
    if coef < 0.0 then
      throw Exception("A spring's stiffness coefficient cannot be negative")
    if damping_ratio < 0.0 then
      throw Exception("A spring's damping ratio cannot be negative")

    val c = coef
    val d = damping_ratio
    val m = mass
    val T = target
    val P = pos
    val V = vel

    /*
      p(x) - spring position at time x

      p(0) = P
      p'(0) = V
      k = 2 * d * sqrt(c * m)  // damping coef
      p''(x) * m = c * (T - p(x)) - k * p'(x)

      solved with SageMath; WolframAlpha didn't do the job...
    */

    if c == 0.0 || !is_active then
      return (P + V * delta_time, V)

    else if d == 0.0 then
      val S = displacement
      val w = sqrt(c / m)

      val cos = math.cos(w * delta_time)
      val sin = math.sin(w * delta_time)
      return (S * -cos     + V * sin / w + T,
              S *  sin * w + V * cos)

    else if d == 1.0 then
      val S = displacement
      val w = sqrt(c / m)

      val A = V - S * w

      val e = exp(-w * delta_time)
      return ((A * delta_time - S) * e + T,
              ((A * delta_time - S) * -w + A) * e)

    else if d > 1.0 then
      val S = displacement
      val k = 2.0 * d * sqrt(c * m)

      val a = sqrt(pow(k, 2.0) - 4.0 * c * m)
      val r2 = (-k - a) / 2.0 / m
      val r1 = (-k + a) / 2.0 / m
      val A = (S * r1 + V) * m / a
      val B = (S * r2 + V) * m / a

      val e1 = exp(r1 * delta_time)
      val e2 = exp(r2 * delta_time)
      return (B * e1      - A * e2 + T,
              B * e1 * r1 - A * e2 * r2)

    else if d < 1.0 then
      val S = displacement
      val w = sqrt(c / m)
      val k = 2.0 * d * sqrt(c * m)

      val g = 2.0 * w
      val h = k / m
      val a = sqrt((g - h) * (g + h)) / 2.0
      val b = -k / 2.0 / m
      val C = (S * b + V) / a

      val e = exp(b * delta_time)
      val sin = math.sin(a * delta_time)
      val cos = math.cos(a * delta_time)
      return ((C * sin - S * cos) * e + T,
              ((C * b + S * a) * sin + (C * a - S * b) * cos) * e)
    else
      throw Exception(s"What the hell is d (${d})?")

  def step(delta_time: Double)(using Ops[A]) =
    val after_step = predict(delta_time)
    pos = after_step._1
    vel = after_step._2

class AngleSpring(_coef: Double, _damping_ratio: Double,
                  _mass: Double, _is_active: Boolean,
                  _target: Double, _pos: Double, _vel: Double)
    extends Spring[Double](_coef, _damping_ratio, _mass, _is_active, _target, _pos, _vel):

  override def displacement(using Ops[Double]) =
    val displacement_ccw = (target - pos) mod (Pi * 2.0)
    val displacement_cw = displacement_ccw - Pi * 2.0
    if displacement_ccw < Pi then displacement_ccw else displacement_cw
