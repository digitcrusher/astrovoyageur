/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.math

import collection.mutable
import math.{acos, cos, Pi, pow, sin}

var pascal = mutable.IndexedBuffer[IndexedSeq[Int]]()
def binomial(n: Int, k: Int): Int =
  if k > n || k < 0 then
    throw Exception("Invalid binomial lower term")

  for i <- pascal.size to n do
    val row = IndexedSeq.newBuilder[Int]
    row += 1
    for j <- 1 to i - 1 do
      row += pascal(i - 1)(j - 1) + pascal(i - 1)(j)
    row += 1
    pascal(i) = row.result

  pascal(n)(k)

def bezier_at(t: Double, points: Iterable[Vec2]): Vec2 =
  val n = points.size
  if n == 0 then
    throw Exception("Got no control points")

  points.zipWithIndex.map(
    (point, i) => point * pow(t, i) * pow(1 - t, n - i - 1) * binomial(n - 1, i)
  ).reduce(_ + _)

def circle_fit_to_two_points_and_radius(pos1: Vec2, pos2: Vec2, radius: Double): (Vec2, Vec2, Double) =
  if pos1 == pos2 then
    throw Exception("Two equal points have an infinite number of fitting circles")

  // the two points and the center of the circle make an isosceles triangle A
  // calculate the base of triangle A
  val dist = pos1.dist_to(pos2)

  if dist > 2.0 * radius then
    throw Exception("Distance between two points must not be greater than the diameter of the circle")

  // if we split triangle A in half we get a right triangle B where
  // the length of the hypotenuse is equal to the length of the leg of triangle A and
  // the length of one of the legs is equal to one half of the length of the base of triangle A
  // calculate triangle A's inner angles
  val base_angle = acos((dist / 2.0) / radius)
  val point_angle = (Pi - base_angle) / 2.0

  // next we make a right triangle C whose legs are parallel to the grid
  // and the hypotenuse is bounded by point one and the center of the circle
  // then we make a line parallel to the x axis that passes through point one
  // and calculate the angle between it and point two
  val axis_angle = pos1.angle_to(pos2)

  // in case one we see that the angle in triangle C supplements baseangle and axisangle
  // in case two the angle supplements baseangle and 90 degress - axisangle
  val third_angle_1 = Pi - base_angle - axis_angle
  val third_angle_2 = Pi - base_angle - (Pi / 2.0 - axis_angle)

  // knowing the hypotenuse and one of the angles in triangle C
  // we can calculate the other sides
  val a1 = radius * cos(third_angle_1)
  val b1 = radius * sin(third_angle_1)

  val a2 = radius * sin(third_angle_2)
  val b2 = radius * cos(third_angle_2)

  val inner_angle = Pi - base_angle * 2.0
  (pos1 + Vec2(-a1, b1), pos2 + Vec2(a2, -b2), inner_angle)

// TODO: circle_fit_to_two_points_and_angle
