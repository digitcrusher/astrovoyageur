/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.math

case class Rect(left: Double, bottom: Double, right: Double, top: Double):
  def width = right - left
  def height = top - bottom

  def start = Vec2(left, bottom)
  def end = Vec2(right, top)
  def center = Vec2((left + right) / 2.0, (bottom + top) / 2.0)
  def vertices = Seq(Vec2(left, bottom), Vec2(right, bottom), Vec2(right, top), Vec2(left, top))

  def fixed = Rect(left min right, bottom min top, left max right, bottom max top)

  def +(vec: Vec2) = Rect(left + vec.x, bottom + vec.y, right + vec.x, top + vec.y)
  def -(vec: Vec2) = Rect(left - vec.x, bottom - vec.y, right - vec.x, top - vec.y)
  def *(scalar: Double) = Rect(left * scalar, bottom * scalar, right * scalar, top * scalar)

  def is_in_rect(other: Rect) = other.left < left && right < other.right &&
                                other.bottom < bottom && top < other.top
  def is_in_or_on_rect(other: Rect) = other.left <= left && right <= other.right &&
                                      other.bottom <= bottom && top <= other.top
  def does_overlap_rect(other: Rect) = !(left >= other.right || right <= other.left ||
                                         bottom >= other.top || top <= other.bottom)
  def does_overlap_or_on_rect(other: Rect) = !(left > other.right || right < other.left ||
                                               bottom > other.top || top < other.bottom)
