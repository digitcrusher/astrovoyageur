/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.math

import math.{atan2, cos, hypot, Pi, sin}

// For Christmas, I want value classes in the JVM
case class Vec2(x: Double, y: Double):
  def mag = hypot(x, y)
  def mag_sqr = x * x + y * y
  def angle =
    var result = atan2(y, x)
    if result < 0.0 then
      result += Pi * 2.0
    result

  def unary_- = Vec2(-x, -y)
  def unit = if mag == 0.0 then this else Vec2(x / mag, y / mag)

  def +(scalar: Double) = Vec2(x + scalar, y + scalar)
  def -(scalar: Double) = Vec2(x - scalar, y - scalar)
  def *(scalar: Double) = Vec2(x * scalar, y * scalar)
  def /(scalar: Double) = Vec2(x / scalar, y / scalar)

  def +(other: Vec2) = Vec2(x + other.x, y + other.y)
  def -(other: Vec2) = Vec2(x - other.x, y - other.y)
  def *(other: Vec2) = Vec2(x * other.x, y * other.y)

  infix def dot(other: Vec2) = x * other.x + y * other.y
  infix def cross(other: Vec2) = x * other.y - y * other.x

  def dist_to(other: Vec2) = hypot(other.x - x, other.y - y)
  def sqr_dist_to(other: Vec2) = (other.x - x) * (other.x - x) + (other.y - y) * (other.y - y)
  def angle_to(other: Vec2) =
    var result = atan2(other.y - y, other.x - x)
    if result < 0.0 then
      result += Pi * 2.0
    result

  def rotated_by_90 = Vec2(-y, x)
  def rotated_by_180 = Vec2(-x, -y)
  def rotated_by_270 = Vec2(y, -x)
  def rotated_by(angle: Double) =
    val sin = math.sin(angle)
    val cos = math.cos(angle)
    Vec2(x * cos + y * -sin,
         x * sin + y *  cos)

  def rotated_around(pivot: Vec2, angle: Double) =
    val sin = math.sin(angle)
    val cos = math.cos(angle)
    val rel_x = x - pivot.x
    val rel_y = y - pivot.y
    Vec2(rel_x * cos + rel_y * -sin + pivot.x,
         rel_x * sin + rel_y *  cos + pivot.y)

  def is_in_rect(rect: Rect) = rect.left < x && x < rect.right &&
                               rect.bottom < y && y < rect.top
  def is_in_or_on_rect(rect: Rect) = rect.left <= x && x <= rect.right &&
                                     rect.bottom <= y && y <= rect.top

  def is_on_rect(rect: Rect) =
    val in_x = rect.left <= x && x <= rect.right
    val in_y = rect.bottom <= y && y <= rect.top
    val on_x = x == rect.left || x == rect.right
    val on_y = y == rect.bottom || y == rect.top
    (on_x && in_y) || (on_y && in_x)

  def is_on_segment(a: Vec2, b: Vec2) =
    if a.x == b.x then
      (a.y min b.y) <= y && y <= (a.y max b.y)
    else
      (a.x min b.x) <= x && x <= (a.x max b.x) && ((b - a) cross (this - a)) == 0.0

object Vec2:
  val empty: Vec2 = null
  val zero = Vec2(0.0, 0.0)
  val origin = zero
  def polar(mag: Double, angle: Double) = Vec2(mag * cos(angle), mag * sin(angle))
  def polar(angle: Double) = Vec2(cos(angle), sin(angle))
