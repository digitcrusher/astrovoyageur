/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.math

import collection.mutable
import math.{cos, Pi, sin}

import ludmotoro.utils.CircularList

// TODO: convexize

def triangulate(simple_polygon: IterableOnce[Vec2]) =
  val result = Seq.newBuilder[(Vec2, Vec2, Vec2)]

  val points = CircularList.from(simple_polygon)

  if points.nodeIterator.map(x => x.value cross x.next.value).sum < 0.0 then
    points.reverse()

  def is_ear(node: points.Node) =
    val a = node.prev.value
    val b = node.value
    val c = node.next.value
    (b - a cross c - b) >= 0.0 && !points.nodeIterator.exists: x =>
      val xa = a - x.value
      val xb = b - x.value
      val xc = c - x.value
      (xa cross xb) > 0.0 && (xb cross xc) > 0.0 && (xc cross xa) > 0.0

  val ears = mutable.Set.from(points.nodeIterator.withFilter(is_ear))
  while ears.nonEmpty && points.size >= 3 do
    val ear = ears.head
    ears -= ear

    result.addOne((ear.prev.value, ear.value, ear.next.value))

    val a = ear.prev
    val b = ear.next
    ear.remove()
    if ears.contains(a) && !is_ear(a) then
      ears -= a
    else if !ears.contains(a) && is_ear(a) then
      ears += a
    if ears.contains(b) && !is_ear(b) then
      ears -= b
    else if !ears.contains(b) && is_ear(b) then
      ears += b

  result.result

enum Side:
  case Left, Right

def ellipse(pos: Vec2, radius_x: Double, radius_y: Double, pointc: Int, should_make_base_flat: Boolean = true) =
  if pointc < 2 then
    throw Exception("Cannot construct an ellipse from less than two points")

  val angle_offset = if should_make_base_flat then Pi / pointc - Pi / 2.0 else 0.0
  Iterator.unfold(0.0): angle =>
    if angle < Pi * 2.0 then
      val x = radius_x * cos(angle + angle_offset) + pos.x
      val y = radius_y * sin(angle + angle_offset) + pos.y
      Some(Vec2(x, y), angle + Pi * 2.0 / pointc)
    else
      None

def circle_from_radius(pos: Vec2, radius: Double, pointc: Int, should_make_base_flat: Boolean = true) =
  ellipse(pos, radius, radius, pointc, should_make_base_flat)

def circle_from_sidelen(pos: Vec2, side_len: Double, pointc: Int, should_make_base_flat: Boolean = true) =
  val radius = side_len / (2.0 * sin(Pi / pointc))
  ellipse(pos, radius, radius, pointc, should_make_base_flat)

def arc(pos: Vec2, radius: Double, start_angle: Double, end_angle: Double, pointc: Int) =
  if pointc < 2 then
    throw Exception("An arc cannot be constructed from less than two points")

  Iterator.tabulate(pointc): i =>
    val angle = start_angle + (end_angle - start_angle) * i / (pointc - 1)
    Vec2(radius * cos(angle) + pos.x,
         radius * sin(angle) + pos.y)

def arc_from_limits(pos: Vec2, limit1: Vec2, limit2: Vec2, pointc: Int) =
  val dist1 = pos.dist_to(limit1)
  val dist2 = pos.dist_to(limit2)
  if dist1 != dist2 then
    throw Exception("The two points are not equidistant from the arc center")

  arc(pos, dist1, pos.angle_to(limit1), pos.angle_to(limit2), pointc)

def arc_from_ends_and_radius(pos1: Vec2, pos2: Vec2, radius: Double,
                             side: Side, pointc: Int,
                             should_draw_longer_side: Boolean = false) =

  val (circle1, circle2, inner_angle) = circle_fit_to_two_points_and_radius(pos1, pos2, radius)

  var pos = Vec2.empty
  var start_angle = 0.0
  var end_angle = 0.0
  side match
    case Side.Left =>
      pos = circle1
      start_angle = pos.angle_to(pos1)
      end_angle = start_angle + inner_angle
      if should_draw_longer_side then
        end_angle -= Pi * 2.0
    case Side.Right =>
      pos = circle2
      start_angle = pos.angle_to(pos1)
      end_angle = start_angle - inner_angle
      if should_draw_longer_side then
        end_angle += Pi * 2.0

  arc(pos, radius, start_angle, end_angle, pointc)

def bezier(points: Iterable[Vec2], pointc: Int) =
  if pointc < 2 then
    throw Exception("A Bézier curve cannot be constructed from less than two points")

  Iterator.unfold(0.0)(
    t => if t < 1.0 then Some(bezier_at(t, points), t + 1.0 / (pointc - 1)) else None
  ) ++ Some(bezier_at(1.0, points))
