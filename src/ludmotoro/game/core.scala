/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.game

import collection.mutable
import reflect.{ClassTag, classTag}

import ludmotoro.utils.now

abstract class Object:
  var scene: Scene = null
  var time = 0.0
  var is_deleted = false
  def ok = !is_deleted
  var priority = 0

  def delete() =
    if !is_deleted then
      is_deleted = true
      scene = null
      // Unfortunately "by design", we cannot immediately remove deleted objects from the scene

  def update(delta_time: Double) =
    time += delta_time

  def draw(delta_time: Double) = {}

open class Scene(var time_scale: Double = 1.0, var ups: Double = 60.0,
                 var dps: Double = 60.0, var update_skip_time: Double = 0.1)
    extends Object:

  var objects = mutable.ListBuffer[Object]()
  val objects_by_class = mutable.Map[Class[?], mutable.ListBuffer[Object]]()
  var pending_objects = mutable.Queue[Object]()
  var last_update = 0.0
  var last_draw = 0.0
  var is_running = true

  override def delete() =
    delete_objects() // Scenes have full ownership over their objects
    super.delete()

  override def update(delta_time: Double) =
    if !is_running then return
    last_update += delta_time

    if pending_objects.nonEmpty then
      for i <- pending_objects do
        i.scene = this
        objects += i
        for (klass, objects) <- objects_by_class do
          if klass.isAssignableFrom(i.getClass) then
            objects += i
      pending_objects.clear()
      objects = objects.sortBy(_.priority)

    objects.filterInPlace(!_.is_deleted)
    for (_, objects) <- objects_by_class do
      objects.filterInPlace(!_.is_deleted)

    val time_per_update = 1.0 / ups
    if last_update >= time_per_update then
      update_forcefully(time_per_update)
      last_update -= time_per_update

      if last_update >= update_skip_time then
        println(f"skipping ${last_update}%.2fs worth of updates")
        last_update = 0.0

  override def draw(delta_time: Double) =
    if !is_running then return
    last_draw += delta_time

    if last_draw >= 1.0 / dps then
      draw_forcefully(last_draw)
      last_draw = 0.0

  def update_forcefully(delta_time: Double) =
    time += delta_time * time_scale
    for i <- objects if i.ok do
      i.update(delta_time * time_scale)

  def draw_forcefully(delta_time: Double) =
    for i <- objects if i.ok do
      i.draw(delta_time)

  def loop() =
    var last_time = now
    while is_running do
      val curr_time = now
      update(curr_time - last_time)
      draw(curr_time - last_time)
      last_time = curr_time

  def objects_of[A <: Object: ClassTag] =
    val klass = classTag[A].runtimeClass
    objects_by_class.getOrElseUpdate(klass, objects.filter(x => klass.isAssignableFrom(x.getClass))).asInstanceOf[Iterable[A]]

  def add_object(obj: Object): Unit =
    pending_objects += obj

  def add_objects(objects: Object*): Unit =
    pending_objects ++= objects

  def delete_objects() =
    for i <- objects if i.ok do
      i.delete()
