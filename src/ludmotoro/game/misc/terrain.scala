/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.game.misc

import collection.mutable
import math.{atan2, Pi}
import util.Random

import ludmotoro.game.renderers
import ludmotoro.game.physics.{Body, Contact, Line, Shape, UnsupportedCollisionError}
import ludmotoro.graphics.{Colors, DrawShapes, Polygon}
import ludmotoro.math.{Rect, Transforms2, Vec2}

def normal_of(pos1: Vec2, pos2: Vec2) =
  if pos1 == pos2 then
    Random.between(0.0, Pi * 2.0) // HACK: hmmmm
  else
    atan2(pos2.y - pos1.y, pos2.x - pos1.x) + Pi / 2.0

def slope_of(pos1: Vec2, pos2: Vec2) = (pos2.y - pos1.y) / (pos2.x - pos1.x).abs

enum Side:
  case Left, Right

class TerrainShape(val terrain: Terrain, var mass: Double = 1.0, var inertia: Double = 1.0) extends Shape:
  override def aabb = Rect(terrain.nodes(0).x, terrain.min_y,
                           terrain.nodes.last.x, terrain.max_y) + terrain.pos
  override def center_of_mass = abs_pos

  override def contacts_with(shape: Shape) =
    val contacts = Seq.newBuilder[Contact]

    val l_node_idx = terrain.section_at(shape.aabb.left)._1
    val r_node_idx = terrain.section_at(shape.aabb.right)._2
    for i <- l_node_idx until r_node_idx do
      val node1 = terrain.pos + terrain.nodes(i)
      val node2 = terrain.pos + terrain.nodes(i + 1)

      if !(shape.aabb.bottom > (node1.y max node2.y) || shape.aabb.top < (node1.y min node2.y)) then
        val normal = terrain.normal_of((i, i + 1))
        val is_start_solid = i - 1 >= 0 && terrain.normal_of((i - 1, i)) - normal > 0.0
        val is_end_solid = i + 2 <= terrain.nodes.size - 1 &&
                          normal - terrain.normal_of((i + 1, i + 2)) > 0.0
        val line = Line(node1, node2, is_start_solid = is_start_solid, is_end_solid = is_end_solid)

        try
          contacts ++= line.contacts_with(shape).map(_.copy(a = this, b = shape))
        catch case _: UnsupportedCollisionError =>
          contacts ++= shape.contacts_with(line).map(x => x.copy(
            a = shape, b = this,
            normal_for_a = x.normal_for_b,
            normal_for_b = x.normal_for_a
          ))

    contacts.result

// terrain generators should have seperate states for both sides
abstract class TerrainGen:
  var terrain: Terrain = null
  def calc_node(side: Side): Vec2

// TODO: add color and witdth (for each section)
// HACK: add a section class
class Terrain(var _terrain_gen: TerrainGen, _restitution: Double) extends Body(restitution = _restitution, is_static = true):
  var nodes = mutable.ArrayDeque(Vec2.origin)
  var min_y = 0.0
  var max_y = 0.0

  def terrain_gen = _terrain_gen
  def terrain_gen_=(value: TerrainGen) =
    if terrain_gen != null then
      terrain_gen.terrain = null
    _terrain_gen = value
    terrain_gen.terrain = this
  terrain_gen = _terrain_gen

  shape = Some(TerrainShape(this))

  override def draw(delta_time: Double) =
    for renderer <- scene.renderers[DrawShapes] do
      val viewport = renderer.viewport

      val l_node_idx = section_at(viewport.left)._1
      val r_node_idx = section_at(viewport.right)._2
      val slice = nodes.slice(l_node_idx, r_node_idx + 1)

      renderer.draw(Polygon.from_polyline(slice, Colors.white, 0.0, 1.0, Transforms2.move(pos)))

  def add_node(node: Vec2, side: Side) =
    // IDEA: automatize the process of inserting nodes into self.nodes using binary search
    side match
      case Side.Left =>
        nodes.prepend(node)
      case Side.Right =>
        nodes += node

    if node.y < min_y then
      min_y = node.y
    if node.y > max_y then
      max_y = node.y

  def gen_node(side: Side) =
    add_node(terrain_gen.calc_node(side), side)

  def section_at(pos_x: Double): (Int, Int) =
    while pos_x < nodes(0).x + pos.x do
      gen_node(Side.Left)
    while pos_x > nodes.last.x + pos.x do
      gen_node(Side.Right)

    var left = 0
    var right = nodes.size - 1
    while true do
      val middle = left + (right - left + 1) / 2
      val node_x = nodes(middle).x + pos.x

      if node_x == pos_x then
        return (middle, middle)
      else if right - left + 1 <= 2 then
        return (left, right)
      else if node_x < pos_x then
        left = middle
      else if node_x > pos_x then
        right = middle

    throw Exception("This will never be reached and you will never see this message")

  def alt_at(pos_x: Double, section: Option[(Int, Int)] = None): Double =
    val (a, b) = section.getOrElse(section_at(pos_x))
    val Vec2(x1, y1) = nodes(a) + pos
    val Vec2(x2, y2) = nodes(b) + pos
    if x1 == x2 then y1 max y2 else (pos_x - x1) * (y2 - y1) / (x2 - x1) + y1

  def normal_of(section: (Int, Int)) = ludmotoro.game.misc.normal_of(nodes(section._1), nodes(section._2))
  def slope_of(section: (Int, Int)) = ludmotoro.game.misc.slope_of(nodes(section._1), nodes(section._2))

class SimpleTerrainGen(val min_slope: Double, val max_slope: Double,
                       val smoothness: Double, val node_spacing: Double,
                       val min_y: Option[Double] = None, val max_y: Option[Double] = None,
                       val force_y_limits: Boolean = false,
                       var left_rand: Random = Random(),
                       var right_rand: Random = Random(),
                       var should_get_nodes_from_terrain: Boolean = true)
    extends TerrainGen:

  var left_last_node_1 = Vec2.origin
  var left_last_node_2: Option[Vec2] = None
  var right_last_node_1 = Vec2.origin
  var right_last_node_2: Option[Vec2] = None

  def calc_node(side: Side): Vec2 =
    // a terrain object is guaranteed to always have at least one node
    val last_node =
      side match
        case Side.Left => if should_get_nodes_from_terrain then terrain.nodes.head else left_last_node_1
        case Side.Right => if should_get_nodes_from_terrain then terrain.nodes.last else right_last_node_1

    val min_slope = min_y.fold(this.min_slope)(y => (y - last_node.y) / node_spacing max this.min_slope min this.max_slope)
    val max_slope = max_y.fold(this.max_slope)(y => (y - last_node.y) / node_spacing min this.max_slope max this.min_slope)

    val rand =
      side match
        case Side.Left => left_rand
        case Side.Right => right_rand
    var slope = if min_slope != max_slope then rand.between(min_slope, max_slope) else min_slope

    val last_slope =
      if should_get_nodes_from_terrain then
        if terrain.nodes.size >= 2 then
          side match
            case Side.Left => terrain.slope_of((1, 0))
            case Side.Right => terrain.slope_of((terrain.nodes.size - 2, terrain.nodes.size - 1))
        else
          slope
      else
        side match
          case Side.Left => left_last_node_2.fold(slope)(slope_of(_, left_last_node_1))
          case Side.Right => right_last_node_2.fold(slope)(slope_of(_, right_last_node_1))
    slope = slope * (1.0 - smoothness) + last_slope * smoothness

    var y = last_node.y + slope * node_spacing
    if force_y_limits then
      if y < min_y.get then
        y = min_y.get
      else if y > max_y.get then
        y = max_y.get

    val x =
      last_node.x + node_spacing * (
        side match
          case Side.Left => -1.0
          case Side.Right => 1.0
      )

    side match
      case Side.Left =>
        left_last_node_2 = Some(left_last_node_1)
        left_last_node_1 = Vec2(x, y)
      case Side.Right =>
        right_last_node_2 = Some(right_last_node_1)
        right_last_node_1 = Vec2(x, y)

    Vec2(x, y)

abstract class Biome:
  var parent: BiomeTerrainGen = null

  def calc_node(side: Side): Vec2
  def next_biome(side: Side): Option[Biome]
  def reset(side: Side): Unit

class BiomeTerrainGen(var left_biome: Biome, var right_biome: Biome,
                      var left_rand: Random = Random(),
                      var right_rand: Random = Random())
    extends TerrainGen:

  left_biome.parent = this
  right_biome.parent = this

  def calc_node(side: Side) =
    side match
      case Side.Left =>
        val node = left_biome.calc_node(Side.Left)
        left_biome.next_biome(Side.Left).foreach: x =>
          x.parent = this
          left_biome.reset(Side.Left)
          left_biome = x
        node

      case Side.Right =>
        val node = right_biome.calc_node(Side.Right)
        right_biome.next_biome(Side.Right).foreach: x =>
          x.parent = this
          right_biome.reset(Side.Right)
          right_biome = x
        node

class SimpleBiome(var terrain_gen: TerrainGen, var next_biomes: IndexedSeq[Biome],
                  var min_nodec: Int, var max_nodec: Int,
                  var left_rand: Random = Random(),
                  var right_rand: Random = Random())
    extends Biome:

  var left_leftc = left_rand.between(min_nodec, max_nodec + 1)
  var right_leftc = right_rand.between(min_nodec, max_nodec + 1)
  var left_next: Biome = null
  var right_next: Biome = null

  def calc_node(side: Side): Vec2 =
    side match
      case Side.Left =>
        if left_leftc <= 0 then
          throw Exception("No nodes left to generate on the left side")

      case Side.Right =>
        if right_leftc <= 0 then
          throw Exception("No nodes left to generate on the right side")

    terrain_gen.terrain = parent.terrain
    val node = terrain_gen.calc_node(side)
    side match
      case Side.Left =>
        left_leftc -= 1
        if left_leftc <= 0 then
          left_next = next_biomes(left_rand.between(0, next_biomes.size))

      case Side.Right =>
        right_leftc -= 1
        if right_leftc <= 0 then
          right_next = next_biomes(right_rand.between(0, next_biomes.size))

    node

  def next_biome(side: Side) =
    side match
      case Side.Left => if left_leftc == 0 then Some(left_next) else None
      case Side.Right => if right_leftc == 0 then Some(right_next) else None

  def reset(side: Side): Unit =
    side match
      case Side.Left =>
        left_leftc = left_rand.between(min_nodec, max_nodec + 1)
        left_next = null
      case Side.Right =>
        right_leftc = right_rand.between(min_nodec, max_nodec + 1)
        right_next = null
