/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.game.misc

import collection.mutable

import ludmotoro.game.{Object, renderers}
import ludmotoro.graphics.{Color, Colors, DrawShapes, Polygon}
import ludmotoro.math.Vec2

// TODO: add smooth node decay and creation
case class TrajectoryNode(pos: Vec2, time: Double)

class Trajectory(var node_limit: Option[Int] = None, var decay_time: Option[Double] = None,
                 var node_delta_time: Option[Double] = None,
                 var color: Color = Colors.white, var width: Double = 1.0)
    extends Object:

  var nodes = mutable.Queue[TrajectoryNode]()

  override def update(delta_time: Double) =
    time += delta_time
    decay()

  override def draw(delta_time: Double): Unit =
    if nodes.nonEmpty then
      for renderer <- scene.renderers[DrawShapes] do
        renderer.draw(Polygon.from_polyline(nodes.map(_.pos), color, width))

  def add_node(pos: Vec2, force: Boolean = false, time: Double = time) =
    if force || nodes.isEmpty || node_delta_time.forall(time - nodes.last.time >= _) then
      nodes += TrajectoryNode(pos, time)

  def clear() =
    nodes.clear()

  def decay(): Unit =
    nodes.dropWhileInPlace(head => node_limit.exists(nodes.size > _) || decay_time.exists(time - head.time >= _))
