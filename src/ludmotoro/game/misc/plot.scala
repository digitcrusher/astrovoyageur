/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.game.misc

import math.{pow, sqrt}

import ludmotoro.game.{Object, renderers}
import ludmotoro.graphics.{Color, Colors, DrawShapes, Polygon}
import ludmotoro.math.{Rect, Vec2}

abstract class Plot(var color: Color = Colors.white, var width: Double = 1.0) extends Object:
  override def draw(delta_time: Double) =
    for renderer <- scene.renderers[DrawShapes] do
      for (left, right) <- sections_to_plot_for(renderer.viewport) do
        val points = Seq.newBuilder[Vec2]

        var x = left
        while x < right do
          points += Vec2(x, height_at(x))
          x += dist_to_next_at(x)
        points += Vec2(right, height_at(right))

        renderer.draw(Polygon.from_polyline(points.result, color, width))

  def height_at(pos_x: Double): Double
  def dist_to_next_at(pos_x: Double): Double
  def sections_to_plot_for(viewport: Rect): Iterable[(Double, Double)] = Some((viewport.left, viewport.right))

class TrajectoryPlot(var pos: Vec2, var vel: Vec2, var acc: Double,
                     var min_point_dist: Double, var point_delta_time: Double,
                     _color: Color = Colors.white, _width: Double = 1.0)
    extends Plot(_color, _width):

  override def height_at(pos_x: Double) =
    if vel.x == 0.0 then
      throw Exception("Predicted trajectory undefined for zero horizontal velocity")

    val a = acc / pow(vel.x, 2) / 2.0
    val b = vel.y / vel.x
    val c = pos.y
    a * pow(pos_x - pos.x, 2) + b * (pos_x - pos.x) + c

  override def dist_to_next_at(pos_x: Double) = min_point_dist max (vel.x.abs * point_delta_time)

  override def sections_to_plot_for(viewport: Rect) =
    val result: Iterable[(Double, Double)] =
      if acc == 0.0 then
        Some((viewport.left, viewport.right))
      else
        if vel.x == 0.0 then
          throw Exception("Predicted trajectory plot undefined for zero horizontal velocity")

        val a = acc / pow(vel.x, 2) / 2.0
        val b = vel.y / vel.x
        val c = pos.y

        def solve_quadratic(a: Double, b: Double, c: Double) =
          val root1 = (-b + sqrt(pow(b, 2) - 4.0 * a * c)) / (2.0 * a)
          val root2 = (-b - sqrt(pow(b, 2) - 4.0 * a * c)) / (2.0 * a)
          (root1, root2)

        val soln1 = solve_quadratic(a, b, c - viewport.top)
        val soln2 = solve_quadratic(a, b, c - viewport.bottom)

        val roots = Seq(soln1._1 + pos.x, soln1._2 + pos.x, soln2._1 + pos.x, soln2._2 + pos.x).filter(!_.isNaN).sorted
        roots.dropRight(roots.size % 2).grouped(2).map(x => (x(0), x(1))).toSeq

    def fix(section: (Double, Double)) =
      var (left, right) = section

      left = left max viewport.left
      right = right min viewport.right

      if vel.x > 0 then
        left = left max pos.x
      else
        right = right min pos.x

      (left, right)

    result.map(fix)
