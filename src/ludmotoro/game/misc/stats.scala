/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.game.misc

import collection.SeqMap

import ludmotoro.game.Scene
import ludmotoro.game.physics.PhysScene
import ludmotoro.graphics.Renderer
import ludmotoro.utils.{Counter, Freeze, Label, Stats, Timer}

def create_scene_stats(scene: Scene, renderer: Renderer) = Stats(SeqMap(
  "ups" -> Freeze(Counter("update", Some("post_print")), "pre_print"),
  "dps" -> Freeze(Counter("draw", Some("post_print")), "pre_print"),
  "scene" -> Label(() => f"${scene.time}%.2fs"),
  "wall" -> Timer(prec = Some(2)),
  "object count" -> Label(() => s"${scene.objects.size}"),
  "draw count" -> Label(() => s"${renderer.drawc}/${renderer.dispatchc}"),
), cycle = 1.0)

def create_phys_scene_stats(scene: PhysScene, renderer: Renderer) = Stats(SeqMap(
  "ups" -> Freeze(Counter("update", Some("post_print")), "pre_print"),
  "dps" -> Freeze(Counter("draw", Some("post_print")), "pre_print"),
  "scene" -> Label(() => f"${scene.time}%.2fs"),
  "wall" -> Timer(prec = Some(2)),
  "object count" -> Label(() => s"${scene.objects.size}"),
  "draw count" -> Label(() => s"${renderer.drawc}/${renderer.dispatchc}"),
  "broad checks" -> Label(() => s"${scene.broad_checkc}"),
  "narrow checks" -> Label(() => s"${scene.narrow_checkc}"),
), cycle = 1.0)
