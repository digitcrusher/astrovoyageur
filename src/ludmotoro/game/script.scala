/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.game

import ludmotoro.graphics.{Button, Event, Renderer, WindowLike, WindowState}
import ludmotoro.math.Vec2

abstract class Script extends Object:
  var has_started = false
  var has_stopped = false

  override def delete() =
    if has_started && !has_stopped then
      stop()

    super.delete()

  def start() =
    has_started = true
    on_start()

  def stop() =
    if has_started then
      on_stop()
    has_stopped = true
    if !is_deleted then
      delete()

  override def update(delta_time: Double) =
    time += delta_time
    if !has_started then
      start()
    on_update(delta_time)

  override def draw(delta_time: Double) =
    if has_started then
      on_draw(delta_time)

  def on_start() = {}
  def on_stop() = {}
  def on_update(delta_time: Double) = {}
  def on_draw(delta_time: Double) = {}

abstract class InteractiveScript extends Script:
  override def update(delta_time: Double) =
    super.update(delta_time)
    for renderer <- scene.renderers[WindowLike] do
      for i <- renderer.events do
        handle_event(i, renderer)

  def handle_event(event: Event, renderer: Renderer & WindowLike) =
    event match
      case Event.Close                 => on_close(event.past, renderer)
      case Event.Resize(size)          => on_resize(size, event.past, renderer)
      case Event.Focus                 => on_focus(event.past, renderer)
      case Event.Unfocus               => on_unfocus(event.past, renderer)
      case Event.ButtonPress(button)   => on_button_press(button, event.past, renderer)
      case Event.ButtonRelease(button) => on_button_release(button, event.past, renderer)
      case Event.MouseMove(pos)        => on_mouse_move(pos, event.past, renderer)
      case Event.MouseScroll(scroll)   => on_mouse_scroll(scroll, event.past, renderer)
      case Event.MouseEnter            => on_mouse_enter(event.past, renderer)
      case Event.MouseLeave            => on_mouse_leave(event.past, renderer)
      case Event.Char(codepoint)       => on_char(codepoint, event.past, renderer)

  def on_close(past: WindowState, renderer: Renderer & WindowLike) = {}
  def on_resize(size: Vec2, past: WindowState, renderer: Renderer & WindowLike) = {}
  def on_focus(past: WindowState, renderer: Renderer & WindowLike) = {}
  def on_unfocus(past: WindowState, renderer: Renderer & WindowLike) = {}
  def on_button_press(button: Button, past: WindowState, renderer: Renderer & WindowLike) = {}
  def on_button_release(button: Button, past: WindowState, renderer: Renderer & WindowLike) = {}
  def on_mouse_move(pos: Vec2, past: WindowState, renderer: Renderer & WindowLike) = {}
  def on_mouse_scroll(scroll: Double, past: WindowState, renderer: Renderer & WindowLike) = {}
  def on_mouse_enter(past: WindowState, renderer: Renderer & WindowLike) = {}
  def on_mouse_leave(past: WindowState, renderer: Renderer & WindowLike) = {}
  def on_char(codepoint: Int, past: WindowState, renderer: Renderer & WindowLike) = {}

class ExampleScript(val name: String, val duration: Double) extends Script:
  var updatec = 0
  var drawc = 0

  override def on_start() =
    println(s"Hello, my name is ${name}!")

  override def on_stop() =
    println(s"${name} leaves after ${updatec} updates and ${drawc} draws")

  override def on_update(delta_time: Double) =
    updatec += 1
    if time >= duration then
      stop()

  override def on_draw(delta_time: Double) =
    drawc += 1
