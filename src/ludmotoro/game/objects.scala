/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.game

import collection.AbstractIterator

import ludmotoro.graphics.{Camera, Renderer}

class RendererObject(var self: Renderer) extends Object

extension (scene: Scene)
  inline def renderers[A]: Iterator[Renderer & A] = new AbstractIterator:
    val it = scene.objects_of[RendererObject].iterator

    var curr: Option[Renderer & A] = None

    def hasNext =
      while curr.isEmpty && it.hasNext do
        it.next.self match
          case x: A =>
            curr = Some(x)
      curr.nonEmpty

    def next =
      while curr.isEmpty do
        it.next.self match
          case x: A =>
            curr = Some(x)
      val result = curr.get
      curr = None
      result

class CameraObject(var self: Camera) extends Object:
  override def update(delta_time: Double) =
    self.update(delta_time)
