/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.game.physics

import math.Pi
import util.boundary, boundary.break
import util.Random

import ludmotoro.game.{Object, Scene}
import ludmotoro.math.{Rect, Vec2}

class UnsupportedCollisionError(_msg: String) extends Exception(_msg)

case class Contact(a: Shape, b: Shape, point: Vec2, normal_for_a: Vec2, normal_for_b: Vec2, penetration: Double)

abstract class Shape(var pos: Vec2 = Vec2.origin, var ori: Double = 0.0):
  var body: Option[Body] = None

  def abs_pos = body.fold(pos)(x => pos.rotated_by(x.ori) + x.pos)
  def abs_ori = body.fold(ori)(_.ori + ori)

  def mass: Double
  def inertia: Double
  def aabb: Rect
  def center_of_mass: Vec2
  def contacts_with(shape: Shape): Iterable[Contact]

case class Collision(a: Body, b: Body, contacts: Iterable[Contact])

// IDEA: add is_ori_fixed
// BUG: the inertia and ang_moment calcs are probably wrong
// HACK: make this a mixin
open class Body(var _shape: Option[Shape] = None,
                var restitution: Double = 1.0,
                var pos: Vec2 = Vec2.origin,
                var ori: Double = 0.0,
                var is_static: Boolean = false)
    extends Object:

  def shape = _shape
  def shape_=(value: Option[Shape]) =
    shape.foreach:
      _.body = None
    _shape = value
    shape.foreach:
      _.body = Some(this)
  shape = _shape

  var lin_momentum = Vec2.zero
  var ang_momentum = 0.0

  def vel = lin_momentum / mass
  def vel_=(value: Vec2) =
    lin_momentum = value * mass
  def rot = ang_momentum / inertia
  def rot_=(value: Double) =
    ang_momentum = value * inertia

  def apply_impulse(impulse: Vec2, pivot: Vec2 = Vec2.origin) =
    lin_momentum += impulse

  def apply_torque(torque: Double) =
    ang_momentum += torque

  def integrate(delta_time: Double) =
    pos += vel * delta_time
    ori += rot * delta_time

  def mass = shape.fold(1.0)(_.mass)
  def inertia = shape.fold(1.0)(_.inertia)
  def aabb = shape.map(_.aabb)
  def center_of_mass = shape.fold(pos)(_.center_of_mass)

  def collision_with(other: Body) =
    if shape.isDefined && other.shape.isDefined then
      Collision(this, other, shape.get.contacts_with(other.shape.get))
    else
      Collision(this, other, Nil)

  def should_collide_with(other: Body) = true
  def on_pre_collision(collision: Collision) = {}
  def on_post_collision(collision: Collision) = {}

// HACK: add support for sweep axis changing
enum Axis:
  case X, Y

open class PhysScene(val sweep_axis: Axis, _time_scale: Double = 1.0,
                     _ups: Double = 60.0, _dps: Double = 60.0,
                     _update_skip_time: Double = 0.1)
    extends Scene(_time_scale, _ups, _dps, _update_skip_time):

  var broad_checkc = 0
  var narrow_checkc = 0

  def integrate(delta_time: Double) =
    for i <- objects_of[Body] if i.ok do
      i.integrate(delta_time)

  // BUG: bodies under gravity gain speed when colliding with 1.0 restitution because of static collision resolution
  // BUG: jiggle caused by the static collision solver
  // TODO: rotational physics
  // TODO: friction
  // TODO: joints
  def resolve_collision(collision: Collision): Unit =
    if collision.contacts.isEmpty then return

    val a = collision.a
    val b = collision.b

    if !a.should_collide_with(b) || !b.should_collide_with(a) then return

    if a.is_static && b.is_static then
      a.on_pre_collision(collision)
      b.on_pre_collision(collision)
      a.on_post_collision(collision)
      b.on_post_collision(collision)
      return

    a.on_pre_collision(collision)
    b.on_pre_collision(collision)

    for contact <- collision.contacts do
      val e = a.restitution min b.restitution
      var n_a = contact.normal_for_a
      var n_b = contact.normal_for_b

      var j_a = ((b.vel - a.vel) dot -n_b max 0.0) * (1.0 + e)
      var j_b = ((a.vel - b.vel) dot -n_a max 0.0) * (1.0 + e)

      var sep = contact.penetration
      if !a.is_static && !b.is_static then
        // equations shamelessly stolen from Randy Gaul's "How to Create a Custom 2D Physics Engine" tutorial series
        j_a /= (1.0 / a.mass + 1.0 / b.mass)
        j_b /= (1.0 / a.mass + 1.0 / b.mass)
        sep /= 2.0
      else
        j_a *= a.mass
        j_b *= b.mass

      // Push perfectly overlapping bodies away along a random direction
      if n_a == Vec2.zero && n_b == Vec2.zero then
        n_a = Vec2.polar(Random.between(0.0, Pi * 2.0))
        n_b = -n_a

      if !a.is_static then
        a.apply_impulse(n_a * j_a, contact.point)
        a.pos += n_a * sep
      if !b.is_static then
        b.apply_impulse(n_b * j_b, contact.point)
        b.pos += n_b * sep

    a.on_post_collision(collision)
    b.on_post_collision(collision)

  def detect_collisions() =
    broad_checkc = 0
    narrow_checkc = 0

    // HACK: preserve this over detect_collisions calls
    val bodies = objects_of[Body]
      .withFilter(_.aabb.nonEmpty)
      .map(x => (x.aabb.get, x))
      .toVector
      .sortBy: (aabb, _) =>
        sweep_axis match
          case Axis.X => aabb.left
          case Axis.Y => aabb.bottom

    for ((aabb1, body1), i) <- bodies.zipWithIndex if body1.ok do
      boundary:
        for (aabb2, body2) <- bodies.drop(i + 1) if body2.ok do // Vector.drop seems to be the fastest option
          if sweep_axis == Axis.X && aabb2.left > aabb1.right ||
             sweep_axis == Axis.Y && aabb2.bottom > aabb1.top then
            break()

          if body1.should_collide_with(body2) && body2.should_collide_with(body1) then
            broad_checkc += 1
            if aabb1.does_overlap_or_on_rect(aabb2) then
              narrow_checkc += 1

              val collision =
                try
                  body1.collision_with(body2)
                catch case _: UnsupportedCollisionError =>
                  body2.collision_with(body1)
              resolve_collision(collision)

  def collisions_with(body1: Body) =
    // HACK: use the sweep and prune optimization in detect_collisions
    val result = Seq.newBuilder[Collision]

    body1.aabb.foreach: aabb1 =>
      for body2 <- objects_of[Body] if body2.ok do
        body2.aabb.foreach: aabb2 =>
          if body1.should_collide_with(body2) && body2.should_collide_with(body1) then
            if aabb1.does_overlap_or_on_rect(aabb2) then
              val collision =
                try
                  body1.collision_with(body2)
                catch case _: UnsupportedCollisionError =>
                  body2.collision_with(body1)
              if collision.contacts.nonEmpty then
                result += collision

    result.result
