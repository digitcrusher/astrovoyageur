/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.game.physics

import math.{cos, Pi, pow, sin, sqrt}

import ludmotoro.math.{Rect, Vec2}

// TODO: implement a polygon shape
// IDEA: add a polyline shape
// HACK: add abs_pos/abs_ori setters

class Point(_pos: Vec2, var mass: Double = 1.0, var inertia: Double = 1.0) extends Shape(_pos):
  override def aabb = Rect(abs_pos.x, abs_pos.y,
                           abs_pos.x, abs_pos.y)
  override def center_of_mass = abs_pos

  override def contacts_with(shape: Shape) =
    shape match
      case point: Point =>
        if abs_pos == point.abs_pos then
          Some(Contact(this, shape, abs_pos, Vec2.zero, Vec2.zero, 0.0))
        else
          Nil

      case _ =>
        throw UnsupportedCollisionError(s"Unsupported collision checking between ${getClass} and ${shape.getClass}")

class Ray(_pos: Vec2, _ori: Double, var mass: Double = 1.0, var inertia: Double = 1.0) extends Shape(_pos, _ori):
  override def aabb =
    val x = cos(abs_ori)
    val y = sin(abs_ori)
    Rect(if x < 0.0 then Double.NegativeInfinity else 0.0,
         if y < 0.0 then Double.NegativeInfinity else 0.0,
         if x > 0.0 then Double.PositiveInfinity else 0.0,
         if y > 0.0 then Double.PositiveInfinity else 0.0) + abs_pos
  override def center_of_mass = abs_pos

  override def contacts_with(shape: Shape) =
    shape match
      case point: Point =>
        // HACK: implement ray-point collision

        Some(Contact(this, shape, point.abs_pos, -Vec2.polar(abs_ori), Vec2.polar(abs_ori), 0.0))

      case ray: Ray => Nil // HACK: implement ray-ray collision
      case _ =>
        throw UnsupportedCollisionError(s"Unsupported collision checking between ${getClass} and ${shape.getClass}")

class Line(var pos1: Vec2, var pos2: Vec2,
           var mass: Double = 1.0, var inertia: Double = 1.0,
           var is_start_solid: Boolean = true, var is_end_solid: Boolean = true)
    extends Shape:

  def abs_pos1 =
    val result = pos1.rotated_by(ori) + pos
    body.fold(result)(x => result.rotated_by(x.ori) + x.pos)
  def abs_pos2 =
    val result = pos2.rotated_by(ori) + pos
    body.fold(result)(x => result.rotated_by(x.ori) + x.pos)

  override def aabb =
    val pos1 = abs_pos1
    val pos2 = abs_pos2
    Rect(pos1.x min pos2.x, pos1.y min pos2.y,
         pos1.x max pos2.x, pos1.y max pos2.y)
  override def center_of_mass = (abs_pos1 + abs_pos2) / 2.0

  override def contacts_with(shape: Shape) =
    shape match
      case point: Point =>
        if !point.abs_pos.is_on_segment(abs_pos1, abs_pos2) then
          Nil
        else
          val normal_for_a = (abs_pos2 - abs_pos1).unit.rotated_by_90
          val normal_for_b = -normal_for_a

          Some(Contact(this, shape, point.abs_pos, normal_for_a, normal_for_b, 0.0))

      case ray: Ray => Nil // HACK: implement line-ray collision
      case line: Line => Nil // HACK: implement line-line contacts retrieving
      case _ =>
        throw UnsupportedCollisionError(s"Unsupported collision checking between ${getClass} and ${shape.getClass()}")

class Circle(var radius: Double, var density: Double = 1.0,
             _pos: Vec2 = Vec2.origin, _ori: Double = 0.0)
    extends Shape(_pos, _ori):

  def area = Pi * radius * radius
  override def mass = area * density
  override def inertia = mass * radius * radius / 2.0
  override def aabb =
    val pos = abs_pos
    Rect(pos.x - radius, pos.y - radius,
         pos.x + radius, pos.y + radius)
  override def center_of_mass = abs_pos

  override def contacts_with(shape: Shape) =
    shape match
      case point: Point =>
        val pos_a = abs_pos
        val pos_b = point.abs_pos

        val dist = pos_a.dist_to(pos_b)
        if dist > radius then
          return Nil

        val normal_for_a = (pos_a - pos_b).unit
        val normal_for_b = -normal_for_a
        Some(Contact(this, shape, (pos_a + pos_b) / 2.0, normal_for_a, normal_for_b, radius - dist))

      case ray: Ray =>
        // r - ray
        // c - circle
        // p1, p2 - closer and farther intersection points

        val r = ray.abs_pos
        val c = abs_pos
        val r_angle = ray.abs_ori

        val angle_prc = r_angle - r.angle_to(c)
        val sqr_dist_rc = r.sqr_dist_to(c)
        val sqr_radius_c = pow(radius, 2)

        // equations from WolframAlpha
        val x = sqr_dist_rc * pow(cos(angle_prc), 2) - sqr_dist_rc + sqr_radius_c
        if x < 0 then
          return Nil
        val a = sqrt(sqr_dist_rc) * cos(angle_prc)
        val b = sqrt(x)
        val dist_rp1 = a - b
        val dist_rp2 = a + b
        val penetration = 2 * b

        val p1 = r + Vec2.polar(dist_rp1, r_angle)
        val p2 = r + Vec2.polar(dist_rp2, r_angle)

        val angle_cp2 = c.angle_to(p2)
        if sqr_dist_rc >= sqr_radius_c then
          val angle_cp1 = c.angle_to(p1)
          Seq(
            Contact(this, shape, p1, -Vec2.polar(angle_cp1), Vec2.polar(angle_cp1), 0.0),
            Contact(this, shape, p2, Vec2.polar(angle_cp2), -Vec2.polar(angle_cp2), penetration),
          )
        else
          Some(Contact(this, shape, p2, Vec2.polar(angle_cp2), -Vec2.polar(angle_cp2), 0.0))

      case line: Line =>
        // s - start
        // e - end
        // c - circle
        // p - closest point

        val s = line.abs_pos1
        val e = line.abs_pos2
        val c = abs_pos

        val dist_se = s.dist_to(e)
        val angle_se = s.angle_to(e)
        val dist_sc = s.dist_to(c)
        val angle_cse = s.angle_to(c) - angle_se
        val dist_sp = dist_sc * cos(angle_cse)

        var point = Vec2.empty
        var normal_for_a = Vec2.empty
        var normal_for_b = Vec2.empty
        var penetration = 0.0
        if dist_sp < 0.0 then
          if dist_sc > radius || !line.is_start_solid then
            return Nil

          point = s
          normal_for_a = (c - s).unit
          normal_for_b = -normal_for_a
          penetration = radius - dist_sc
        else if dist_sp > dist_se then
          val dist_ec = e.dist_to(c)
          if dist_ec > radius || !line.is_end_solid then
            return Nil

          point = e
          normal_for_a = (c - e).unit
          normal_for_b = -normal_for_a
          penetration = radius - dist_ec
        else
          val dist_pc = dist_sc * sin(angle_cse)

          if dist_pc.abs > radius then
            return Nil

          point = s + Vec2.polar(dist_sp, angle_se)
          normal_for_a = if dist_pc >= 0.0 then (e - s).unit.rotated_by_90 else (e - s).unit.rotated_by_270
          normal_for_b = -normal_for_a
          penetration = radius - dist_pc.abs

        Some(Contact(this, shape, point, normal_for_a, normal_for_b, penetration))

      case circle: Circle =>
        val pos_a = abs_pos
        val pos_b = circle.abs_pos

        val dist = pos_a.dist_to(pos_b)
        val radii_sum = radius + circle.radius

        if dist > radii_sum then
          return Nil

        val point = (pos_a + pos_b) / 2.0
        val normal_for_a = (pos_a - pos_b).unit
        val normal_for_b = -normal_for_a
        val penetration = radii_sum - dist

        Some(Contact(this, shape, point, normal_for_a, normal_for_b, penetration))

      case _ =>
        throw UnsupportedCollisionError(s"Unsupported collision checking between ${getClass} and ${shape.getClass}")
