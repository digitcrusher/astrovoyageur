/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.utils

import collection.AbstractIterator

extension [A](xs: IterableOnce[A])
  def lookahead: Iterator[(A, Option[A])] = new AbstractIterator:
    val it = xs.iterator

    var curr = it.nextOption
    def hasNext = curr.nonEmpty
    def next =
      val result = (curr.get, it.nextOption)
      curr = result._2
      result

  def circular_pairs: Iterator[(A, A)] = new AbstractIterator:
    val it = xs.iterator

    val first = it.nextOption
    var curr = first
    def hasNext = curr.nonEmpty
    def next =
      val prev = curr
      curr = it.nextOption
      (prev.get, curr.getOrElse(first.get))

extension [A](xs: Iterable[A])
  def circular_neighbors: Iterator[(A, A, A)] = new AbstractIterator:
    val it = xs.iterator

    val first = it.nextOption
    var prev = xs.lastOption
    var curr = first
    def hasNext = curr.nonEmpty
    def next =
      val next = it.nextOption
      val result = (prev.get, curr.get, next.getOrElse(first.get))
      prev = curr
      curr = next
      result
