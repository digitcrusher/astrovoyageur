/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.utils

abstract class Stat:
  def value: String
  def update(msg: String): Unit

class Label(var value_func: () => String) extends Stat:
  def value = value_func()
  def update(msg: String) = {}

class Counter(var tick_msg: String, var reset_msg: Option[String] = None) extends Stat:
  var count = 0
  def tick() =
    count += 1
  def reset() =
    count = 0

  def value = s"${count}"
  def update(msg: String) =
    if msg == tick_msg then
      tick()
    if reset_msg.contains(msg) then
      reset()

class Timer(var reset_msg: Option[String] = None, var time: Double = now, var prec: Option[Int] = None) extends Stat:
  def reset() =
    time = now

  def value = prec.fold("%fs")(x => s"%.${x}fs").format(now - time)
  def update(msg: String) =
    if reset_msg.contains(msg) then
      reset()

class Freeze(var stat: Stat, var melt_msg: String) extends Stat:
  var frozen = ""
  def melt() =
    frozen = stat.value

  def value = frozen
  def update(msg: String) =
    stat.update(msg)
    if msg == melt_msg then
      melt()

class Stats(var stats: Map[String, Stat], var cycle: Double,
            var pre_print_msg: String = "pre_print",
            var post_print_msg: String = "post_print"):

  def string = stats.map((name, stat) => s"${name}: ${stat.value}").mkString(", ")

  var last_print = now
  def print(): Unit =
    if now - last_print >= cycle then
      last_print = now
      update_all(pre_print_msg, false)
      println(string)
      update_all(post_print_msg, false)

  def update(name: String, msg: String) =
    stats(name).update(msg)
    print()

  def update_all(msg: String, should_print: Boolean = true) =
    for i <- stats.values do
      i.update(msg)
    if should_print then
      print()
