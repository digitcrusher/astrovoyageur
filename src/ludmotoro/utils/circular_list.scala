/*
 * Astrovoyageur - A 2D moon landing game
 * Copyright (C) 2020-2023 Karol "digitcrusher" Łacina
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ludmotoro.utils

import collection.AbstractIterator

class CircularList[A]:
  class Node(val value: A, var prev: Node, var next: Node):
    def remove() =
      size -= 1
      if size == 0 then
        head = null
      else if this == head then
        head = next
      prev.next = next
      next.prev = prev
      prev = null
      next = null

  var head: Node = null
  var size = 0

  def nodeIterator: Iterator[Node] = new AbstractIterator:
    var curr = head

    def hasNext = curr != null
    def next =
      val result = curr
      curr = curr.next
      if curr == head then
        curr = null
      result

  def reverse() =
    for i <- nodeIterator do
      val temp = i.next
      i.next = i.prev
      i.prev = temp

object CircularList:
  def from[A](xs: IterableOnce[A]) =
    val result = CircularList[A]()

    val it = xs.iterator
    if it.nonEmpty then
      result.head = result.Node(it.next, null, null)
      result.size = 1

      var last = result.head
      for i <- it do
        val node = result.Node(i, last, null)
        last.next = node
        last = node
        result.size += 1

      result.head.prev = last
      last.next = result.head

    result
