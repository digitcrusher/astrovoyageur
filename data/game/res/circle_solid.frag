#version 110

void main() {
  vec2 pos = gl_TexCoord[0].xy * 2.0 - 1.0;
  float dist_sqr = dot(pos, pos);
  float delta = fwidth(dist_sqr) / 2.0;
  float alpha = smoothstep(1.0 + delta, 1.0 - delta, dist_sqr);
  gl_FragColor = vec4(gl_Color.rgb, gl_Color.a * alpha);
}
