import java.time.{format, OffsetDateTime}

val os = "linux" // or "windows" or "macos"

scalaVersion := "3.3.1"
name := "astrovoyageur"
version := OffsetDateTime.now.format(format.DateTimeFormatter.ofPattern("yyyy-MM-dd_HH.mm.ss"))

Compile / scalaSource := baseDirectory.value / "src"
Compile / resourceDirectory := baseDirectory.value / "data/game"

artifactName := ((sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
                 s"${artifact.name}-${module.revision}.${artifact.extension}")

libraryDependencies ++= Seq(
  "lwjgl",
  "lwjgl-glfw",
  "lwjgl-opengl",
  "lwjgl-stb",
).flatMap(module => Seq(
  "org.lwjgl" % module % "3.3.3",
  "org.lwjgl" % module % "3.3.3" classifier s"natives-${os}",
))

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", _) => MergeStrategy.discard
  case _ => MergeStrategy.first
}

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  "-new-syntax",
  "-source:future",
  "-Wnonunit-statement",
  "-Wunused:all",
  "-Wvalue-discard",
)
